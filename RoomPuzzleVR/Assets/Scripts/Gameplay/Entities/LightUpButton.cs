﻿using UnityEngine;
using UnityEngine.Events;
using HTC.UnityPlugin.ColliderEvent;
using HTC.UnityPlugin.Utility;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class LightUpButton : MaterialEntity, IColliderEventHoverEnterHandler, IColliderEventHoverExitHandler
    {
        public UnityEvent OnFlashStart = new UnityEvent();
        public UnityEvent OnFlashEnd = new UnityEvent();


        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void StartFlashing()
        {
            OnFlashStart.Invoke();
            ChangeState(EntityState.Magenta);

            //Debug.Log(this.name);
        }

        public void StopFlashing()
        {
            OnFlashEnd.Invoke();
            ChangeState(EntityState.Neutral);
        }

        public void OnColliderEventHoverEnter(ColliderHoverEventData eventData)
        {
            m_levelMechanics.touchedButton(m_ID);
            StartFlashing();
        }

        public void OnColliderEventHoverExit(ColliderHoverEventData eventData)
        {
            StopFlashing();
        }

        public void Reset(int p_ID, Level2CMechanics p_mechanics)
        {
            m_ID = p_ID;
            m_levelMechanics = p_mechanics;
        }

        private Level2CMechanics m_levelMechanics;

        private int m_ID;
    }
}