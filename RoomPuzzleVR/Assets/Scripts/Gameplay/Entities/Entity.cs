﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Entity : MonoBehaviour
    {
        public event System.Action<EntityState, EntityState> OnStateChange;
        public UnityEvent OnCreated = new UnityEvent();
        public UnityEvent OnDestroyed = new UnityEvent();
       
        
        [Header("Entity")]
        [SerializeField] private EntityState m_StartingState = EntityState.Neutral;
        [SerializeField] private bool m_InitializeOnStart = false;

        public EntityState CurrentState
        {
            get
            {
                return m_currentState;
            }
        }

        public void ChangeState(EntityState p_nextState)
        {
            if (OnStateChange != null)
            {
                var l_oldState = m_currentState;
                m_currentState = p_nextState;

                OnStateChange.Invoke(l_oldState, p_nextState);
            }
            else
            {
                m_currentState = p_nextState;
            }
        }

        public virtual void Init()
        {
            m_currentState = m_StartingState;
            if (null != OnCreated)
                OnCreated.Invoke();
        }

        public virtual void Destroy()
        {
            if (null != OnDestroyed)
                OnDestroyed.Invoke();

            GameObject.Destroy(this.gameObject);
        }

        void Start()
        {
            if(m_InitializeOnStart)
                Init();
        }



        private EntityState m_currentState;
    }

    public enum EntityState
    {

        Neutral = 0,
        Magenta,
        Red,
        Yellow,
        Green,
        Blue,
        Cyan

    }

}
