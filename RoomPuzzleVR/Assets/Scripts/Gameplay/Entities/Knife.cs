﻿using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{

    public class Knife : MaterialEntity, IRigidbodyEntity
    {
        public override void Init()
        {
            Rigidbody l_rb = GetRigidbody();
            l_rb.isKinematic = false;
            l_rb.useGravity = false;

            OnStateChange += (p_prev, p_new) =>
            {
                if(p_prev == EntityState.Neutral && p_new == EntityState.Red)
                {
                    Stick();
                }
            };

            base.Init();
        }

        public void Stick()
        {
            Debug.Log("HIT!");
            Rigidbody l_rb = GetRigidbody();
            l_rb.isKinematic = true;
            l_rb.useGravity = false;
            l_rb.velocity = Vector3.zero;
            l_rb.angularVelocity = Vector3.zero;
            l_rb.drag = float.PositiveInfinity;

        }

        public Rigidbody GetRigidbody()
        {
            if (null == m_rigidbody)
                m_rigidbody = GetComponent<Rigidbody>();
            return m_rigidbody;
        }

        private Rigidbody m_rigidbody;
    }
}