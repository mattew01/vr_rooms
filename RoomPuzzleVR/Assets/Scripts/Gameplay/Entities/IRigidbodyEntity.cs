﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Com.RoomPuzzleVR.Gameplay
{
    public interface IRigidbodyEntity
    {
        Rigidbody GetRigidbody();


    }
}