﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class PushButton : Entity
    {
        public UnityEvent OnPushedDownEvent;
        public UnityEvent OnPushedUpEvent;

    }
}
