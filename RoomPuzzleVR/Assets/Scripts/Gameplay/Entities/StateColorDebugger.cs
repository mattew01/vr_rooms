﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{

    public class StateColorDebugger : MonoBehaviour
    {
        private Entity m_entity;

        private Renderer m_EntityRenderer;

        void OnEnable()
        {
            m_entity = GetComponent<Entity>();
            m_EntityRenderer = GetComponent<Renderer>();

            m_entity.OnStateChange += colorChange;

            //colorChange(EntityState.Neutral, m_entity.CurrentState);
            // needs to be in an editor script
        }

        void OnDisable()
        {
            m_entity.OnStateChange -= colorChange;
            m_entity = null;
            m_EntityRenderer = null;
        }

        void colorChange(EntityState p_old, EntityState p_new)
        {
            switch (p_new)
            {
                case EntityState.Neutral:
                    m_EntityRenderer.material.color = Color.white;
                    break;
                case EntityState.Magenta:
                    m_EntityRenderer.material.color = Color.magenta;
                    break;
                case EntityState.Yellow:
                    m_EntityRenderer.material.color = Color.yellow;
                    break;
                case EntityState.Red:
                    m_EntityRenderer.material.color = Color.red;
                    break;
                case EntityState.Green:
                    m_EntityRenderer.material.color = Color.green;
                    break;
                case EntityState.Blue:
                    m_EntityRenderer.material.color = Color.blue;
                    break;
                case EntityState.Cyan:
                    m_EntityRenderer.material.color = Color.cyan;
                    break;
            }
        }
    }
}
