﻿using UnityEngine;
using System.Collections;

namespace Com.RoomPuzzleVR.Gameplay
{

    public class Robot : MaterialEntity
    {
        [Header("Robot")]
        [SerializeField] private int m_StartingHealth;

        private int m_CurrentHealth;

        public void OnCollisionEnter(Collision collision)
        {
            FoodItem l_foodItem = collision.gameObject.GetComponent<FoodItem>();

            if(null != l_foodItem)
            {
                m_CurrentHealth -= l_foodItem.getDamage();

                StartCoroutine(flashDamage());
            }
        }

        private void Update()
        {
            if(CurrentState == EntityState.Red)
                return;

            if(m_CurrentHealth <= 0)
            {
                ChangeState(EntityState.Red);
            }
        }

        IEnumerator flashDamage()
        {
            ChangeState(EntityState.Magenta);
            yield return new WaitForSeconds(0.25f);
            ChangeState(EntityState.Neutral);
        }

        public override void Init()
        {
            base.Init();

            m_CurrentHealth = m_StartingHealth;
        }
    }
}