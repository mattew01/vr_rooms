﻿using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Ball : MaterialEntity, IRigidbodyEntity
    {

        public override void Init()
        {
            Rigidbody l_rb = GetRigidbody();
            l_rb.isKinematic = false;
            l_rb.useGravity = true;

            base.Init();
        }

        //void FixedUpdate()
        //{
        //    // check if we are moving
        //    Rigidbody l_rb = GetRigidbody();
            
        //    if (Mathf.Abs( l_rb.velocity.magnitude ) < 0.01f)
        //    {
        //        if (CurrentState == EntityState.Neutral)
        //            ChangeState(EntityState.Red);
        //    }
        //    else
        //    {
        //        if (CurrentState == EntityState.Red)
        //            ChangeState(EntityState.Neutral);
        //    }
        //}

        public Rigidbody GetRigidbody()
        {
            if (null == m_rigidbody)
                m_rigidbody = GetComponent<Rigidbody>();
            return m_rigidbody;
        }

        public void ApplyKickForce( Vector3 p_force )
        {
            Rigidbody l_rb = GetRigidbody();
            Debug.Log("Add FORCE: " + p_force);
            l_rb.AddForce(p_force, ForceMode.Impulse);
        }

        private Rigidbody m_rigidbody;
    }
}