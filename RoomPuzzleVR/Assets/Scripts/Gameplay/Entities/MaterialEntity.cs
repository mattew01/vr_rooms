﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class MaterialEntity : Entity
    {
        [SerializeField] private List<MaterialEntry> m_materials;
        [SerializeField] private Renderer m_EntityRenderer;

        void OnEnable()
        {
            if(null == m_EntityRenderer)
                m_EntityRenderer = GetComponent<Renderer>();
            OnStateChange += colorChange;
        }

        void OnDisable()
        {
            OnStateChange -= colorChange;
        }

        void colorChange(EntityState p_old, EntityState p_new)
        {
            var entries = m_materials.Where(e => e.state == p_new);

            foreach (var e in entries)
            {
                m_EntityRenderer.material = e.material;
            }

        }
    }

    [System.Serializable]
    public class MaterialEntry
    {
        public EntityState state;
        public Material material;
    }
}
