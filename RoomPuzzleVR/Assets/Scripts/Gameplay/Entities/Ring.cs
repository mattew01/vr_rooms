﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{ 
    [SelectionBase]
    public class Ring : MaterialEntity, IRigidbodyEntity
    {
        public override void Init()
        {
            Rigidbody l_rb = GetRigidbody();
            l_rb.isKinematic = false;
            l_rb.useGravity = true;

            base.Init();
        }
    
        void FixedUpdate()
        {
            // check if we are moving
            Rigidbody l_rb = GetRigidbody();
            
            if (Mathf.Abs( l_rb.velocity.magnitude ) < 0.01f)
            {
                if (CurrentState == EntityState.Neutral)
                    ChangeState(EntityState.Blue);
            }
            else
            {
                if (CurrentState == EntityState.Blue)
                    ChangeState(EntityState.Neutral);
            }
        }
         

        public Rigidbody GetRigidbody()
        {
            if(null == m_rigidbody)
                m_rigidbody = GetComponent<Rigidbody>();
            return m_rigidbody;
        }

        private Rigidbody m_rigidbody;


    }
}