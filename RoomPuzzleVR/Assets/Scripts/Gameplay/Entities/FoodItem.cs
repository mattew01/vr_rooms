﻿using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class FoodItem : MaterialEntity
    {
        [Header("Foot Item")]
        [SerializeField] private int m_Damage = 1;

        public int getDamage()
        {
            return m_Damage;
        }
    }
}