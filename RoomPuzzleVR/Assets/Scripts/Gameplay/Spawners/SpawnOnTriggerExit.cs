﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class SpawnOnTriggerExit : EntitySpawner
    {
        [SerializeField] protected int m_SpawnDelay;

        private Entity m_lastSpawnedObject;

        private bool m_isSpawning = false;


        void Start()
        {
            m_isSpawning = false;
            StartCoroutine(SpawnPrefab());
        }

        private void OnTriggerExit(Collider other)
        {
            if(m_isSpawning || IsNotActiveInLevel) return;

            if ((other.GetComponent<Entity>() == m_lastSpawnedObject) ||
                (other.GetComponentInParent<Entity>() == m_lastSpawnedObject))
            {
                m_isSpawning = true;
                StopCoroutine(SpawnPrefab());
                if(IsActiveInLevel)
                    StartCoroutine(SpawnPrefab());
            }


            //Debug.Log("Exited: " + other.name);
        }

        private IEnumerator SpawnPrefab()
        {
            

            yield return new WaitForSeconds(m_SpawnDelay);

            GameObject m_obj = Spawn();
            m_obj.name = m_EntityPrefab.name;

            m_lastSpawnedObject = m_obj.GetComponent<Entity>();
            m_lastSpawnedObject.Init();
            m_isSpawning = false;
        }
    }
}