﻿using System.Collections;
using System.Collections.Generic;
using Com.RoomPuzzleVR.Systems;
using UnityEngine;
using UnityEngine.Events;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class EntitySpawner : LevelBehaviour
    {
        [SerializeField] protected GameObject m_EntityPrefab;

        public EntitySpawnEvent OnEntitySpawned;

        public virtual GameObject Spawn()
        {
            if (null == m_EntityPrefab)return null;
            GameObject l_entity = GameObject.Instantiate(m_EntityPrefab, this.transform.position, this.transform.rotation);

            OnEntitySpawned.Invoke(l_entity);
            return l_entity;
        }

    }

    [System.Serializable]
    public class EntitySpawnEvent : UnityEvent<GameObject> {  }

}
