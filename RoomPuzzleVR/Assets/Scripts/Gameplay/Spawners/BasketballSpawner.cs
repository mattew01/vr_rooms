﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{

    public class BasketballSpawner : EntitySpawner
    {

        private Counter m_counter;
        private LevelTimer m_levelTimer;

        void Start()
        {
            m_counter = FindObjectOfType<Counter>();
            m_levelTimer = FindObjectOfType<LevelTimer>();
        }
    }
}
