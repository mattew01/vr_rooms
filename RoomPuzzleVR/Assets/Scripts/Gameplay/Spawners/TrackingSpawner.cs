﻿using System.Collections;
using System.Collections.Generic;
using Com.RoomPuzzleVR.Systems;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{

    public class TrackingSpawner : EntitySpawner
    {
        [Header("Tracking")]
        [SerializeField] protected bool m_RespawnIfDestroyed;
        [SerializeField] protected Observer[] m_Observers;

        private GameObject m_SpawnedEntity = null;
        public override GameObject Spawn()
        {
            m_SpawnedEntity = base.Spawn();

            Entity l_entity = m_SpawnedEntity.GetComponent<Entity>();
            if (null == l_entity)
                return m_SpawnedEntity;
            foreach (var l_observer in m_Observers)
            {
                l_observer.watchEntity(l_entity);
            }
            return m_SpawnedEntity;
        }
    }
}
