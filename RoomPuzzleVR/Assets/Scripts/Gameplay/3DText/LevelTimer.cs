﻿using System.Collections;
using System.Collections.Generic;
using Com.RoomPuzzleVR.Systems;
using UnityEngine;
using UnityEngine.Events;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class LevelTimer : LevelBehaviour
    {
        [Header("Level Timer Details")]
        [SerializeField] private int m_StartingTime = 99;
        [SerializeField] private int m_EndingTime = 0;
        [SerializeField] private bool m_StartCountdownAtStart = true;
        public UnityEvent OnTimerEnded;

        void Start()
        {
            m_TextMesh = this.GetComponent<TextMesh>();

            if (m_StartCountdownAtStart)
                StartTimer();
        }

        public override void OnLevelBegin()
        {
            base.OnLevelBegin();
            StartTimer();

        }

        public override void OnLevelEnd()
        {
            StopTimer();
            base.OnLevelEnd();
        }

        public void StartTimer()
        {
            m_Timer = m_StartingTime;
            m_TimerActive = true;
        }

        public void StopTimer()
        {
            m_TimerActive = false;
        }

        public void PauseTimer()
        {
            m_TimerActive = false;
        }

        public void UnPauseTimer()
        {
            m_TimerActive = true;
        }

        void FixedUpdate()
        {
            if (m_TimerActive)
                m_Timer -= Time.fixedDeltaTime;

            if (m_Timer <= m_EndingTime)
            {
                OnTimerEnded.Invoke();
                StopTimer();
            }
        }

        void Update()
        {
            m_TextMesh.text = Mathf.RoundToInt(m_Timer).ToString();
        }

        private TextMesh m_TextMesh;
        private float m_Timer;
        private bool m_TimerActive = false;
    }
}
