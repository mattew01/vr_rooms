﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Counter : Observer
    {
        private TextMesh m_TextMesh;

        [Header("State Change to Watch")]

        [SerializeField] private EntityState m_PrevState;
        [SerializeField] private EntityState m_NextState;
        public Entity[] m_EntitiesWatched;
        private List<WatchListEntry> m_WatchList = new List<WatchListEntry>();

        [Header("Count Details")]
        [SerializeField] private int m_StartingCount = 0;
        [SerializeField] private int m_EndingCount = 0;
        public enum CountDirection
        {
            COUNT_UP,
            COUNT_DOWN
        }

        [SerializeField] private CountDirection m_CountDirection;
        [SerializeField] private bool m_DisableOnReachedEnd = true;

        public UnityEvent OnReachedEndCount = new UnityEvent();

        public void Count()
        {
            if (m_isCounting == false)return;

            switch (m_CountDirection)
            {
                case CountDirection.COUNT_DOWN:
                    m_Count--;
                    break;
                case CountDirection.COUNT_UP:
                    m_Count++;
                    break;
            }

            m_TextMesh.text = m_Count.ToString();

            if (m_Count == m_EndingCount)
            {
                if (null != OnReachedEndCount)
                    OnReachedEndCount.Invoke();

                if (m_DisableOnReachedEnd)
                    m_isCounting = false;
            }
        }

        public void Reset()
        {
            m_isCounting = true;
            m_Count = m_StartingCount;
            m_TextMesh.text = m_Count.ToString();
        }

        private void Start()
        {
            m_TextMesh = this.GetComponent<TextMesh>();

            Reset();

            foreach (var l_entity in m_EntitiesWatched)
            {
                var l_entry = new WatchListEntry(l_entity, m_PrevState, true, m_NextState, true);
                l_entry.Setup(this);
                m_WatchList.Add(l_entry);
            }
        }

        public override void runListCheck()
        {
            if (IsNotActiveInLevel)return;

            if (m_WatchList.Count <= 0)return;

            bool l_hasOccurred = false;

            foreach (var entry in m_WatchList)
            {
                if (entry.hasOccurred == true)
                {
                    l_hasOccurred = true;
                    entry.Reset();
                    break;
                }
            }

            if (l_hasOccurred)
            {
                Count();
            }
        }

        public override void OnLevelEnd()
        {
            m_isCounting = false;
            base.OnLevelEnd();
        }

        public override void watchEntity(Entity p_entity)
        {
            var l_entry = new WatchListEntry(p_entity, m_PrevState, true, m_NextState, true);
            l_entry.Setup(this);
            m_WatchList.Add(l_entry);
        }

        private bool m_isCounting = false;
        private int m_Count;
    }
}
