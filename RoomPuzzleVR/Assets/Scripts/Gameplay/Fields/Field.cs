﻿using System.Collections;
using System.Collections.Generic;
using Com.RoomPuzzleVR.Systems;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{

    public abstract class Field : LevelBehaviour
    {
        [Header("Field")]
        [SerializeField] protected BoxCollider m_boxCollider;
        [SerializeField] protected SphereCollider m_sphereCollider;

        void OnEnable()
        {
            m_boxCollider = GetComponent<BoxCollider>();
            if (null != m_boxCollider)
                m_boxCollider.isTrigger = true;

            m_sphereCollider = GetComponent<SphereCollider>();
            if (null != m_sphereCollider)
                m_sphereCollider.isTrigger = true;
        }

        void OnDisable()
        {
            m_boxCollider = null;
            m_sphereCollider = null;
        }

        void OnDrawGizmos()
        {
            if (m_boxCollider != null)
            {
                Gizmos.color = new Color(0.1f, 0.7f, 0f, 1f);
                Gizmos.DrawWireCube(
                    new Vector3(m_boxCollider.center.x + transform.position.x,
                        m_boxCollider.center.y + transform.position.y,
                        m_boxCollider.center.z + transform.position.z),
                    new Vector3((m_boxCollider.size.x * transform.localScale.x),
                        m_boxCollider.size.y * transform.localScale.y,
                        m_boxCollider.size.z * transform.localScale.z));
            }
            else if (m_sphereCollider != null)
            {
                Gizmos.color = new Color(0.1f, 0.7f, 0f, 1f);
                Gizmos.DrawWireSphere(new Vector3(m_sphereCollider.center.x + transform.position.x,
                        m_sphereCollider.center.y + transform.position.y,
                        m_sphereCollider.center.z + transform.position.z),
                    m_sphereCollider.radius * transform.localScale.x);
            }
            else
            {
                m_Collider = GetComponent<Collider>();
                if (null != m_Collider)
                {
                    m_boxCollider = m_Collider as BoxCollider;
                    m_sphereCollider = m_Collider as SphereCollider;
                }
            }
        }

        private Collider m_Collider;

    }
}
