﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class TransitonOnExitField : Field
    {
        // Start is called before the first frame update
        [Header("Transition On Exit")]
        [SerializeField] private EntityState NewStateOnExit;
        [SerializeField] private bool IgnorePreviousState = true;
        [SerializeField] private EntityState PreviousStateOnExit;

        /// <summary>
        /// OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerExit(Collider other)
        {
            if (IsNotActiveInLevel)return;

            Entity l_entity = other.GetComponent<Entity>();
            if (null == l_entity)return;

            if (IgnorePreviousState)
            {
                l_entity.ChangeState(NewStateOnExit);
            }
            else
            {
                if (l_entity.CurrentState == PreviousStateOnExit)
                {
                    l_entity.ChangeState(NewStateOnExit);
                }
            }

        }
    }
}
