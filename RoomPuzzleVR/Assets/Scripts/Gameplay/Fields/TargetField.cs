﻿using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class TargetField : Field
    {
        [Header("Target Field")]
        [SerializeField] private EntityState HitState;

        /// <summary>
        /// OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerEnter(Collider other)
        {
            if (IsNotActiveInLevel) return;

            Knife l_entity = other.GetComponent<Knife>();
            if (null == l_entity) return;

            l_entity.transform.SetParent(this.transform);
            l_entity.Stick();

            l_entity.ChangeState(HitState);

        }
    }
}