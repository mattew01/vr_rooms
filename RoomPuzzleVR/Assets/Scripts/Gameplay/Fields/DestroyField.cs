﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class DestroyField : Field
    {
        [Header("Destroy Field")]

        // what states should we destroy this object?
        // if empty, destroy any state
        [SerializeField] private List<EntityState> m_DestroyableStates = new List<EntityState>();

        void OnTriggerEnter(Collider other)
        {
            Entity l_entity = other.GetComponent<Entity>();
   
            if (null == l_entity)
            {
                l_entity = other.GetComponentInParent<Entity>();
                if(null == l_entity)
                    return;
            }

            if (m_DestroyableStates.Count == 0)
            { 
                l_entity.Destroy();
            }
            else
            {
                foreach(var l_state in m_DestroyableStates)
                {
                    if(l_entity.CurrentState == l_state)
                    {
                        l_entity.Destroy();
                        return;
                    }
                }
            }
        }
    }
}
