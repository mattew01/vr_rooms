﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{

    public class TransitionOnEnterField : Field
    {
        [Header("Transition On Enter")]
        [SerializeField] private EntityState NewStateOnEnter;
        [SerializeField] private bool IgnorePreviousState = true;
        [SerializeField] private EntityState PreviousStateOnEnter;

        /// <summary>
        /// OnTriggerEnter is called when the Collider other enters the trigger.
        /// </summary>
        /// <param name="other">The other Collider involved in this collision.</param>
        void OnTriggerEnter(Collider other)
        {
            if (IsNotActiveInLevel)return;

            Entity l_entity = other.GetComponent<Entity>();
            if (null == l_entity)return;

            if (IgnorePreviousState)
            {
                l_entity.ChangeState(NewStateOnEnter);
            }
            else
            {
                if (l_entity.CurrentState == PreviousStateOnEnter)
                {
                    l_entity.ChangeState(NewStateOnEnter);
                }
            }

        }

    }
}
