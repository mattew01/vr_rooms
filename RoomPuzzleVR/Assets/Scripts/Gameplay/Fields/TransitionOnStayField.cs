﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{

    public class TransitionOnStayField : Field
    {
        [Header("Transition On Stay")]
        [SerializeField] private EntityState NewStateOnStay;
        [SerializeField] private bool IgnorePreviousState = true;
        [SerializeField] private EntityState PreviousStateOnStay;


        void OnTriggerStay(Collider other)
        {
            if (IsNotActiveInLevel) return;

            Entity l_entity = other.GetComponent<Entity>();
            if (null == l_entity)
            {
                l_entity = other.GetComponentInParent<Entity>();
                if(null == l_entity)
                {
                    return;
                }
            }

            if(l_entity.CurrentState == NewStateOnStay) return;

            if (IgnorePreviousState)
            {
                l_entity.ChangeState(NewStateOnStay);
            }
            else
            {
                if (l_entity.CurrentState == PreviousStateOnStay)
                {
                    l_entity.ChangeState(NewStateOnStay);
                }
            }

        }

    }
}