﻿using System.Collections;
using System.Collections.Generic;
using Com.RoomPuzzleVR.Systems;
using UnityEngine;
using UnityEngine.Events;

namespace Com.RoomPuzzleVR.Gameplay
{

    public abstract class Observer : LevelBehaviour
    {

        public abstract void runListCheck();

        public abstract void watchEntity(Entity p_entity);

    }

    [System.Serializable]
    public class WatchListEntry
    {
        [SerializeField] private Entity entityObserving;
        [SerializeField] private bool checkLastState = false;
        [SerializeField] private EntityState lastStateObserved;
        [SerializeField] private bool checkNextState = false;
        [SerializeField] private EntityState nextStateObserved;
        public bool hasOccurred { get; private set; }
        private Observer m_observer;

        public WatchListEntry()
        {
            hasOccurred = false;
            checkLastState = false;
            checkNextState = false;
        }

        public WatchListEntry(Entity p_entity, EntityState p_last, bool p_checkLast, EntityState p_next, bool p_checkNext)
        {
            entityObserving = p_entity;
            checkLastState = p_checkLast;
            checkNextState = p_checkNext;
            lastStateObserved = p_last;
            nextStateObserved = p_next;
            hasOccurred = false;
        }

        public void Setup(Observer p_observer)
        {
            m_observer = p_observer;
            entityObserving.OnStateChange += observedStateChange;
        }

        public void Reset()
        {
            hasOccurred = false;
        }

        void observedStateChange(EntityState p_old, EntityState p_new)
        {
            if (p_old != lastStateObserved && p_new != nextStateObserved)return;

            if (checkLastState && checkNextState)
            {
                if (p_old == lastStateObserved && p_new == nextStateObserved)
                {
                    hasOccurred = true;
                    m_observer.runListCheck();
                }
            }
            else if (!checkLastState && checkNextState)
            {
                if (p_new == nextStateObserved)
                {
                    hasOccurred = true;
                    m_observer.runListCheck();
                }
            }
            else if (checkLastState && !checkNextState)
            {
                if (p_old == lastStateObserved)
                {
                    hasOccurred = true;
                    m_observer.runListCheck();
                }
            }
        }
    }
}
