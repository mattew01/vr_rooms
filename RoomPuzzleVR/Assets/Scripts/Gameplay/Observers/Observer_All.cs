﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Observer_All : Observer
    {
        [Header("Event triggers when all observed events occur")]
        public UnityEvent OnWatchedEventOccurance;
        public List<WatchListEntry> m_WatchList = new List<WatchListEntry>();

        public override void runListCheck()
        {
            if (IsNotActiveInLevel)return;
            if (m_WatchList.Count <= 0)return;

            bool l_hasOccurred = true;

            foreach (var entry in m_WatchList)
            {
                if (entry.hasOccurred == false)
                {
                    l_hasOccurred = false;
                    break;
                }
            }

            if (l_hasOccurred)
            {
                OnWatchedEventOccurance.Invoke();
                Debug.Log("OnWatchedEventOccurance has triggered");
            }
        }

        void Start()
        {
            foreach (var l_entry in m_WatchList)
            {
                l_entry.Setup(this);
            }
        }

        public override void watchEntity(Entity p_entity) { }
    }
}
