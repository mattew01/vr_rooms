﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Com.RoomPuzzleVR.Gameplay
{

    public class Observer_Any : Observer
    {
        // Start is called before the first frame update
        [Header("Event triggers when any observed event occurs")]
        public UnityEvent OnWatchedEventOccurance;

        public List<WatchListEntry> m_WatchList = new List<WatchListEntry>();

        public override void runListCheck()
        {
            if (IsNotActiveInLevel)return;

            Debug.Log("runListCheck has triggered");
            if (m_WatchList.Count <= 0)return;

            bool l_hasOccurred = false;

            foreach (var entry in m_WatchList)
            {
                if (entry.hasOccurred == true)
                {
                    l_hasOccurred = true;
                    break;
                }
            }

            if (l_hasOccurred)
            {
                OnWatchedEventOccurance.Invoke();
                Debug.Log("OnWatchedEventOccurance has triggered");
            }
        }

        void Start()
        {
            foreach (var l_entry in m_WatchList)
            {
                l_entry.Setup(this);
            }
        }

        public override void watchEntity(Entity p_entity) { }

    }
}
