﻿using UnityEngine;
using Com.RoomPuzzleVR.Systems;

namespace Com.RoomPuzzleVR.Gameplay
{
    public abstract class GameMechanics : MonoBehaviour
    {
        [Header("General Game Mechanics")]
        [SerializeField] private float m_ReloadTimeAfterLevelFailure = 2f;



        public LevelManager levelManager
        {
            get
            {
                if(null == m_levelManager)
                    m_levelManager = GameObject.FindObjectOfType<LevelManager>();
                return m_levelManager;
            }
        }

        public virtual void StartLevel()
        {
            m_levelCompleted = false;
        }

        public virtual void EndLevel()
        {
            m_levelCompleted = true;
        }

        public abstract GameLevel getLevel();

        public float getReloadTime()
        {
            return m_ReloadTimeAfterLevelFailure;
        }

        public bool IsLevelComplete
        {
            get { return m_levelCompleted; }
        }

        private bool m_levelCompleted;

        private LevelManager m_levelManager;
    }
}
