﻿using UnityEngine;
using Com.RoomPuzzleVR.Systems;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Level2EMechanics : GameMechanics
    {
        public Key m_Key;
        public Pig m_Pig;
        public GameObject m_UnwalkableFloor;
        public GameObject m_WalkableFloor;

        public override GameLevel getLevel()
        {
            return GameLevel.E;
        }

        public override void StartLevel()
        {
            m_Key.Init();
            m_Key.OnStateChange += OnKeyUnlocked;

            m_Pig.Init();
            m_Pig.OnStateChange += OnPigPlaced;

            m_UnwalkableFloor.SetActive(true);
            m_WalkableFloor.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {

        }


        void OnKeyUnlocked( EntityState p_prev, EntityState p_new)
        {
            if( p_prev == EntityState.Neutral && p_new == EntityState.Green)
            {
                m_UnwalkableFloor.SetActive(false);
                m_WalkableFloor.SetActive(true);
            }
            if( p_prev == EntityState.Green && p_new == EntityState.Neutral)
            {
                m_UnwalkableFloor.SetActive(true);
                m_WalkableFloor.SetActive(false);
            }
        }

        
        void OnPigPlaced( EntityState p_prev, EntityState p_new)
        {
            if( p_prev == EntityState.Blue && p_new == EntityState.Cyan)
            {
                levelManager.LevelSuccess();
            }
        }
    }
}