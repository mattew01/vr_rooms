﻿using Com.RoomPuzzleVR.Systems;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    /// <summary>
    /// 2D) PENALTY-KICK SOCCER
    /// Player can kick a ball into a goal, with a goalie attempting to block the shot
    /// - ball needs to detect that it's in the goal -> goal state
    /// - detect missed shot -> dead state
    /// - detect dead ball (no movement) -> stopped state
    /// - goalie track (goalie is just a dummy) -- follows a pre-set path back & forth
    /// - player applies a force to the ball
    /// - ball using gravity & rigidbody physics
    /// - ball needs to spawn after goal/dead ball
    /// - aim line? line renderer
    /// - no movement (locks them into position)
    /// </summary>
    public class Level2DMechanics : GameMechanics
    {
        [Header("References -- Do not edit")]
        [SerializeField] private GameObject m_LeftReticule;
        [SerializeField] private GameObject m_RightReticule;

        private Ball m_activeBall;

        [Header("Level D Mechanics")]
        [SerializeField] private int m_goalsScored = 0;
        [SerializeField] private int m_MaxGoalsToWin = 5;
        [SerializeField] private Vector3 m_ForceAdjustmentFactor;



        // Start is called before the first frame update
        public override void StartLevel()
        {
            m_goalsScored = 0;
        }

        public override GameLevel getLevel()
        {
            return GameLevel.D;
        }


        public void OnBallSpawned(GameObject p_ball)
        {
            m_activeBall = p_ball.GetComponent<Ball>();

            if (m_activeBall != null)
            {
                m_activeBall.OnStateChange += onScoredGoal;
                m_ballKicked = false;
            }
        }

        void onScoredGoal(EntityState p_prev, EntityState p_new)
        {
            if (p_prev == EntityState.Neutral && p_new == EntityState.Green)
            {
                m_goalsScored++;

                if (m_goalsScored >= m_MaxGoalsToWin)
                {
                    levelManager.LevelSuccess();
                }
            }
        }

        [ContextMenu("Kick Ball")]
        public void KickBall()
        {
            if(IsLevelComplete || m_ballKicked) return;

            if (m_RightReticule.activeSelf)
            {
                Vector3 l_force = new Vector3( 
                    m_RightReticule.transform.position.x * m_ForceAdjustmentFactor.x, 
                    m_RightReticule.transform.position.y * m_ForceAdjustmentFactor.y,
                    m_RightReticule.transform.position.z * m_ForceAdjustmentFactor.z);

                m_activeBall.ApplyKickForce(l_force);
                m_ballKicked = true;
            }
            else if (m_LeftReticule.activeSelf)
            {
                Vector3 l_force = new Vector3( 
                    m_LeftReticule.transform.position.x * m_ForceAdjustmentFactor.x, 
                    m_LeftReticule.transform.position.y * m_ForceAdjustmentFactor.y,
                    m_LeftReticule.transform.position.z * m_ForceAdjustmentFactor.z);

                m_activeBall.ApplyKickForce(l_force);
                m_ballKicked = true;
            }
        }

        bool m_ballKicked = false;
    }
}