﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.RoomPuzzleVR.Systems;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Level2CMechanics : GameMechanics
    {
        [Header("Level C Mechanics")]
        public List<LightUpButton> m_ClickableObjects = new List<LightUpButton>();
        private List<int> m_activeSequence;
        public float m_delayBetweenEachElement = 0.5f;
        public float m_delayToFlash = 0.5f;
        public int m_StartingSequenceCount = 4;
        public int m_TotalSequencesToWin = 3;


        public override void StartLevel()
        {
            m_currentWinCount = 0;
            m_currentSequenceLength = m_StartingSequenceCount;

            m_activeSequence = new List<int>();

            GenerateNewSequence(m_currentSequenceLength);

            StartCoroutine(PlaySequence());
        }

        // Update is called once per frame
        void Update()
        {

        }

        public override GameLevel getLevel()
        {
            return GameLevel.C;
        }

        void GenerateNewSequence(int p_length)
        {
            m_currentSequenceIndex = 0;
            m_SequenceSuccess = false;
            m_SequenceFailure = false;

            if (m_ClickableObjects.Count == 0)
            {
                Debug.LogError("m_ClickableObjects is empty");
                return;
            }

            m_activeSequence.Clear();
            for (int i = 0; i < p_length; i++)
            {
                int index = Mathf.FloorToInt(Random.value * (m_ClickableObjects.Count - 0.001f));
                m_activeSequence.Add(index);
            }

            for(int i = 0; i < m_ClickableObjects.Count; i++)
            {
                m_ClickableObjects[i].Reset(i, this);
            }
        }

        IEnumerator PlaySequence()
        {
            if (null != m_activeSequence && m_activeSequence.Count > 0)
            {
                yield return new WaitForSeconds(m_delayBetweenEachElement * 2f);

                foreach (var index in m_activeSequence)
                {
                    yield return new WaitForSeconds(m_delayBetweenEachElement);
                    m_ClickableObjects[index].StartFlashing();
                    yield return new WaitForSeconds(m_delayToFlash);
                    m_ClickableObjects[index].StopFlashing();
                }
            }
        }

        IEnumerator Correct()
        {
            yield return new WaitForSeconds(0.5f);

            for(int i = 0; i < 5; i ++)
            { 
                foreach( var obj in m_ClickableObjects)
                {
                    obj.StartFlashing();
                }
                yield return new WaitForSeconds(0.2f);

                foreach( var obj in m_ClickableObjects)
                {
                    obj.StopFlashing();
                }
                yield return new WaitForSeconds(0.2f);
            }

            Debug.Log("WIN");
            yield return new WaitForSeconds(0.5f);
            if(m_currentWinCount >= m_TotalSequencesToWin)
            {
                levelManager.LevelSuccess();
            }
            else
            {
                m_currentSequenceLength++;
                GenerateNewSequence(m_currentSequenceLength);
                StartCoroutine(PlaySequence());
            }
        }

        IEnumerator Incorrect()
        {
            yield return new WaitForSeconds(0.5f);
            foreach( var obj in m_ClickableObjects)
            {
                obj.StartFlashing();
            }
            yield return new WaitForSeconds(0.9f);
            foreach( var obj in m_ClickableObjects)
            {
                obj.StopFlashing();
            }

             Debug.Log("LOSE");
             levelManager.LevelFailure();
        }

        public void touchedButton(int p_ID)
        {
            if(m_SequenceFailure || m_SequenceSuccess)
                return;

            if(m_currentSequenceIndex >= m_activeSequence.Count)
                return;

            if(m_activeSequence[m_currentSequenceIndex] == p_ID)
            {
                Debug.Log("SUCCESSFUL TOUCH");

                m_currentSequenceIndex++;
                if(m_currentSequenceIndex == m_activeSequence.Count)
                {
                    // WIN
                    Debug.Log("WIN");
                    SequenceSuccess();
                    m_SequenceSuccess = true;
                }
            }
            else
            {
                Debug.Log("LOSE");
                StartCoroutine(Incorrect());
                m_SequenceFailure = true;
            }
        }

        public void SequenceSuccess()
        {
            m_currentWinCount++;
            StartCoroutine(Correct());
        }


        private int m_currentSequenceIndex = 0;
        private int m_currentSequenceLength = 0;
        [SerializeField] private int m_currentWinCount = 0;

        private bool m_SequenceSuccess;
        private bool m_SequenceFailure;
    }
}