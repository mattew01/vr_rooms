﻿using UnityEngine;
using Com.RoomPuzzleVR.Systems;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Level2AMechanics : GameMechanics
    {
        [Header("Level A Mechanics")]
        [SerializeField] private int m_CountToWin = 3;
        [SerializeField] private int m_currentCount = 0;

        public override void StartLevel()
        {
            m_currentCount = 0;
        }

        public override GameLevel getLevel()
        {
            return GameLevel.A;
        }


        public void OnEntitySpawned(GameObject p_object)
        {
            if(null == p_object) return;

            Entity l_entity = p_object.GetComponent<Entity>();

            l_entity.OnStateChange += onSetToGreen;

        }

        void onSetToGreen(EntityState p_prev, EntityState p_new)
        {
            if(p_prev == EntityState.Blue && p_new == EntityState.Green)
            { 
                m_currentCount++;

                if(m_currentCount >= m_CountToWin)
                    levelManager.LevelSuccess();
            }
        }
    }
}