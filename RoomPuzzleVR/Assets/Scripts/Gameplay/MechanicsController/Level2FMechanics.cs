﻿using Com.RoomPuzzleVR.Systems;
using UnityEngine;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Level2FMechanics : GameMechanics
    {
        [Header("Level F Mechanics")]
        [SerializeField] private Robot m_Robot;

        // Start is called before the first frame update
        public override void StartLevel()
        {
            m_Robot.Init();
            m_Robot.OnStateChange += onRobotDeath;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public override GameLevel getLevel()
        {
            return GameLevel.F;
        }

        void onRobotDeath(EntityState p_prev, EntityState p_new)
        {
            if(p_prev == EntityState.Neutral && p_new == EntityState.Red)
            {
                levelManager.LevelSuccess();
            }
        }
    }
}