﻿using UnityEngine;
using Com.RoomPuzzleVR.Systems;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Level2HMechanics : GameMechanics
    {
        [Header("Level H Mechanics")]
        [SerializeField] private int m_RemainingCount = 6;

        // Start is called before the first frame update
        public override void StartLevel()
        {
            m_levelComplete = false;
        }

        // Update is called once per frame
        void Update()
        {
            if(m_levelComplete)
                return;

            if(m_RemainingCount <= 0)
            {
                m_levelComplete = true;
                levelManager.LevelSuccess();
            }
        }

        public override GameLevel getLevel()
        {
            return GameLevel.H;
        }

        public void OnBallDestroyed()
        {
            m_RemainingCount--;
        }

        bool m_levelComplete;
    }
}