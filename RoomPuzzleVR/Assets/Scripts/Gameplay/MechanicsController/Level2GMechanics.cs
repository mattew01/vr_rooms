﻿using UnityEngine;
using Com.RoomPuzzleVR.Systems;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Level2GMechanics : GameMechanics
    {
        [Header("Level G Mechanics")]
        [SerializeField] private int m_HitsToWin = 5;
        [SerializeField] private int m_HitCount = 0;

        // Start is called before the first frame update
        public override void StartLevel()
        {
            m_HitCount = 0;
        }

        // Update is called once per frame
        void Update()
        {
            if(m_HitCount >= m_HitsToWin)
            {
                levelManager.LevelSuccess();
            }
        }

        public override GameLevel getLevel()
        {
            return GameLevel.G;
        }


        public void OnKnifeSpawned(GameObject p_knife)
        {
            Knife l_knife = p_knife.GetComponent<Knife>();

            if(null == l_knife) return;

            l_knife.OnStateChange += (p_prev, p_new) => {
                if(p_prev == EntityState.Neutral && p_new == EntityState.Red)
                {
                    m_HitCount++;
                }
            };
        }
    }
}