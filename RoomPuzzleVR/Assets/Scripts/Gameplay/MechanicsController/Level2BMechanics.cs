﻿using UnityEngine;
using Com.RoomPuzzleVR.Systems;

namespace Com.RoomPuzzleVR.Gameplay
{
    public class Level2BMechanics : GameMechanics
    {
        [Header("Level B Mechanics")]
        [SerializeField] private bool m_FlowerAPlaced;
        [SerializeField] private Flower m_FlowerA;
        [SerializeField] private EntityState m_FlowerAState = EntityState.Green;
        [SerializeField] private bool m_FlowerBPlaced;
        [SerializeField] private Flower m_FlowerB;
        [SerializeField] private EntityState m_FlowerBState = EntityState.Green;
        [SerializeField] private bool m_FlowerCPlaced;
        [SerializeField] private Flower m_FlowerC;
        [SerializeField] private EntityState m_FlowerCState = EntityState.Green;

        private bool m_levelComplete;

        // Start is called before the first frame update
        public override void StartLevel()
        {
            base.StartLevel();

            m_levelComplete = false;
            m_FlowerA.OnStateChange += onFlowerAStateChanged;
            m_FlowerB.OnStateChange += onFlowerBStateChanged;
            m_FlowerC.OnStateChange += onFlowerCStateChanged;
        }

        public override void EndLevel()
        {
            m_FlowerA.OnStateChange -= onFlowerAStateChanged;
            m_FlowerB.OnStateChange -= onFlowerBStateChanged;
            m_FlowerC.OnStateChange -= onFlowerCStateChanged;

            base.EndLevel();
        }

        void Update()
        {
            if(m_levelComplete) return;

            if(m_FlowerAPlaced && m_FlowerBPlaced && m_FlowerCPlaced)
            {
                levelManager.LevelSuccess();
                m_levelComplete = true;
            }
        }

        public override GameLevel getLevel()
        {
            return GameLevel.B;
        }
        
        void onFlowerAStateChanged(EntityState p_prev, EntityState p_new)
        {
            if(p_prev == EntityState.Neutral && p_new == m_FlowerAState)
            {
                m_FlowerAPlaced = true;
            }
            if(p_prev == m_FlowerAState && p_new == EntityState.Neutral)
            {
                m_FlowerAPlaced = false;
            }
        }

        void onFlowerBStateChanged(EntityState p_prev, EntityState p_new)
        {
            if(p_prev == EntityState.Neutral && p_new == m_FlowerBState)
            {
                m_FlowerBPlaced = true;
            }
            if(p_prev == m_FlowerBState && p_new == EntityState.Neutral)
            {
                m_FlowerBPlaced = false;
            }
        }

        void onFlowerCStateChanged(EntityState p_prev, EntityState p_new)
        {
            if(p_prev == EntityState.Neutral && p_new == m_FlowerCState)
            {
                m_FlowerCPlaced = true;
            }
            if(p_prev == m_FlowerCState && p_new == EntityState.Neutral)
            {
                m_FlowerCPlaced = false;
            }
        }
    }
}