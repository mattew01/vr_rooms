﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Com.RoomPuzzleVR.Gameplay
{


public class Patrol : MonoBehaviour
{
        [SerializeField] private List<Transform> m_WayPoints;
        [SerializeField] private float m_TraveTimeToNextWaypoint = 5f;
        [SerializeField] private bool m_PatrolAtStart = false;


        private void Start()
        {
            if(m_PatrolAtStart)
                StartPatrol(0);
        }

        public void StartPatrol(int p_index)
        {
            m_nextWayPointIndex = p_index;

            GoToNextWaypoint();
        }

        public void StopPatrol()
        {
            m_sequence.Kill();
        }

        public void GoToNextWaypoint()
        {
            m_nextWayPointIndex++;
            if(m_nextWayPointIndex >= m_WayPoints.Count)
                m_nextWayPointIndex = 0;

            Transform l_target = m_WayPoints[m_nextWayPointIndex];

            Tween l_move = this.transform.DOMove(l_target.position, m_TraveTimeToNextWaypoint);
            l_move.SetEase(Ease.InOutQuad);

			m_sequence = DOTween.Sequence();
			m_sequence.Append(l_move);
			m_sequence.AppendCallback(GoToNextWaypoint);
			m_sequence.Play();
        }

        private int m_nextWayPointIndex = 0;
        private Sequence m_sequence;
}
}