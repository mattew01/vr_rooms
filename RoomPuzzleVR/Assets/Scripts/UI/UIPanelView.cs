﻿using Com.GameCloud.Controllers.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Com.RoomPuzzleVR.UI
{
    public abstract class UIPanelView : MonoBehaviour
    {
        // --------------------------------------------------------------------
        // Fields & Properties
        // --------------------------------------------------------------------
        [Header ("UI Panel View")]
        [SerializeField] protected Canvas m_canvas;
        [SerializeField] protected UIAnimator m_uiAnimator;

        // --------------------------------------------------------------------
        // Public Methods
        // --------------------------------------------------------------------

        public Canvas GetCanvas ()
        {
            if (null == m_canvas)
                m_canvas = gameObject.GetComponent<Canvas> ();
            return m_canvas;
        }

        public UIAnimator GetUIAnimator ()
        {
            if (null == m_uiAnimator)
                m_uiAnimator = gameObject.GetComponent<UIAnimator> ();
            return m_uiAnimator;
        }

        public void SetActive (bool value)
        {
            this.gameObject.SetActive (value);
        }

        // --------------------------------------------------------------------
        // Private Functions
        // --------------------------------------------------------------------
    }
}