﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class SlideOutToBottom_PanelAnimation : TweenPanelAnimation
	{
		public override void reset()
		{
			base.reset();
			Vector3 l_localPosition = m_tweeningTransform.anchoredPosition3D;
			l_localPosition.y = 0;
			m_tweeningTransform.anchoredPosition3D = l_localPosition;
		}

		protected override void performAnimation()
		{
			base.performAnimation();
			float l_screenHeight = m_rootCanvasTransform.rect.height;
			Tween l_tween = m_tweeningTransform.DOAnchorPosY(-l_screenHeight, m_animationTime, false);
			l_tween.SetEase(Ease.InOutQuad);

			m_sequence = DOTween.Sequence();
			m_sequence.Append(l_tween);
			m_sequence.AppendCallback(TweenPanelAnimation_onComplete);
			m_sequence.Play();
		}
	}
}