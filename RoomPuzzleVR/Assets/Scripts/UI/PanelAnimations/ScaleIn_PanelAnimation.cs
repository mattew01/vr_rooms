﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
    public class ScaleIn_PanelAnimation : TweenPanelAnimation
    {
        public override void reset()
        {
            base.reset();
            /*
            float l_screenWidth = m_screenTransform.rect.width;
            Vector3 l_localPosition = m_tweeningTransform.anchoredPosition3D;
            l_localPosition.x = l_screenWidth;
            m_tweeningTransform.anchoredPosition3D = l_localPosition;
            */
            Vector3 l_scale = m_tweeningTransform.localScale;
            l_scale.y = 0.0f;
            m_tweeningTransform.localScale = l_scale;
        }

        protected override void performAnimation()
        {
            base.performAnimation();
            //Tween l_tween = m_tweeningTransform.DOAnchorPosX(0, m_animationTime, false);

            Tween l_tween = m_tweeningTransform.DOScaleY(1.0f, m_animationTime);

            l_tween.SetEase(Ease.InOutQuad);

            m_sequence = DOTween.Sequence();
            m_sequence.Append(l_tween);
            m_sequence.AppendCallback(TweenPanelAnimation_onComplete);
            m_sequence.Play();
        }
    }
}