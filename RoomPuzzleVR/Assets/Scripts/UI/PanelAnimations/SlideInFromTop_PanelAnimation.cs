﻿using System.Collections;
using Com.GameCloud.Controllers.UI;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class SlideInFromTop_PanelAnimation : TweenPanelAnimation
	{
		public override void reset()
		{
			base.reset();
			float l_screenHeight = m_rootCanvasTransform.rect.height;
			Vector3 l_localPosition = m_tweeningTransform.anchoredPosition3D;
			l_localPosition.y = l_screenHeight;
			m_tweeningTransform.anchoredPosition3D = l_localPosition;
		}

		protected override void performAnimation()
		{
			base.performAnimation();
			Tween l_tween = m_tweeningTransform.DOAnchorPosY(0, m_animationTime, false);
			l_tween.SetEase(Ease.InOutQuad);

			m_sequence = DOTween.Sequence();
			m_sequence.Append(l_tween);
			m_sequence.AppendCallback(TweenPanelAnimation_onComplete);
			m_sequence.Play();
		}
	}
}