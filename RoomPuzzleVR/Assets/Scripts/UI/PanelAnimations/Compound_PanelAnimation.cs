﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	/// <summary>
	/// Combine multiple UI Panel animations together to play in a sequence
	/// </summary>
	public class Compound_PanelAnimation : UIPanelAnimation
	{
		[SerializeField] private List<UIPanelAnimation> m_animations;

		public override void animate()
		{
			base.animate();
			int l_animationCount = m_animations.Count;
			for (int i = l_animationCount - 1; i >= 0; --i)
			{
				UIPanelAnimation l_screenAnimation = m_animations[i];
				l_screenAnimation.animate();
			}
		}

		public override void finish()
		{
			base.finish();
			int l_animationCount = m_animations.Count;
			for (int i = l_animationCount - 1; i >= 0; --i)
			{
				UIPanelAnimation l_screenAnimation = m_animations[i];
				l_screenAnimation.finish();
			}
		}

		public override void stop()
		{
			base.stop();
			int l_animationCount = m_animations.Count;
			for (int i = l_animationCount - 1; i >= 0; --i)
			{
				UIPanelAnimation l_screenAnimation = m_animations[i];
				l_screenAnimation.stop();
			}
		}

		public override float getPercentage()
		{
			float l_progress = 0f;

			int l_animationCount = m_animations.Count;
			for (int i = l_animationCount - 1; i >= 0; --i)
			{
				UIPanelAnimation l_screenAnimation = m_animations[i];
				l_progress += l_screenAnimation.getPercentage();
			}

			if (l_animationCount > 0)
			{
				l_progress /= l_animationCount;
			}

			return l_progress;
		}

		public override void addOnFinishCallback(System.Action p_action)
		{
			base.addOnFinishCallback(p_action);
			if (m_animations.Count > 0)
			{
				UIPanelAnimation l_animation = m_animations[0];
				l_animation.addOnFinishCallback(p_action);
			}
		}

		public override void removeOnFinishCallback(System.Action p_action)
		{
			base.removeOnFinishCallback(p_action);
			if (m_animations.Count > 0)
			{
				UIPanelAnimation l_animation = m_animations[0];
				l_animation.removeOnFinishCallback(p_action);
			}
		}

		protected Sequence m_sequence;
	}
}