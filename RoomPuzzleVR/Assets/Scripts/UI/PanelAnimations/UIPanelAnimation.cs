﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Com.GameCloud.Controllers.UI
{
	public interface IAnimateable
	{
		void animate();
		void finish();
		void stop();
		void reset();
		void addOnFinishCallback(System.Action p_action);
		void removeOnFinishCallback(System.Action p_action);
		float getPercentage();
	}

	public abstract class UIPanelAnimation : MonoBehaviour, IAnimateable
	{
		[SerializeField] protected Image m_tapGuard;
		[SerializeField] protected float m_animationTime = 0.5f;
		[SerializeField] protected bool m_shouldResetBeforeAnimation = true;
		public UnityEvent OnAnimationCompleted = new UnityEvent();

		// ------------------ public interface -----------------

		public virtual void animate()
		{
			setTapGuard(true);
		}

		public virtual void reset() { }

		public virtual void finish()
		{
			setTapGuard(false);
		}

		public virtual void stop()
		{
			setTapGuard(false);
		}

		public virtual void setAnimationTime(float p_time)
		{
			m_animationTime = p_time;
		}

		public virtual void setResetBeforeAnimate(bool p_isResetting)
		{
			m_shouldResetBeforeAnimation = p_isResetting;
		}

		public virtual void addOnFinishCallback(System.Action p_action)
		{
			m_onComplete -= p_action;
			m_onComplete += p_action;
		}

		public virtual void removeOnFinishCallback(System.Action p_action)
		{
			m_onComplete -= p_action;
		}

		public virtual float getPercentage()
		{
			return 0f;
		}

		// ------------------ Protected -----------------

		protected void onAnimationComplete()
		{
			setTapGuard(false);
			m_onComplete();
			OnAnimationCompleted.Invoke();
		}

		protected void setTapGuard(bool p_isEnabled)
		{
			if (null != m_tapGuard)
			{
				m_tapGuard.raycastTarget = p_isEnabled;
			}
		}

		protected event System.Action m_onComplete = delegate { };
	}

}