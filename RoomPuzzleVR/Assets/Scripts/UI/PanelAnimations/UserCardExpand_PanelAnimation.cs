﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class UserCardExpand_PanelAnimation : TweenPanelAnimation
	{
		[SerializeField] public float m_expandTime = 0.5f;
		[SerializeField] public float m_fadeTime = 0.25f;
		[SerializeField] public float m_timeBeforeFadeIn = 0.3f;
		[SerializeField] public RectTransform m_smallView;
		[SerializeField] public RectTransform m_card;
		[SerializeField] public RectTransform m_compositeBack;
		[SerializeField] public RectTransform m_actions;

		public override void reset()
		{
			base.reset();
			m_tweeningTransform.sizeDelta = Vector2.zero;
			m_tweeningCanvasGroup.alpha = 0;
		}

		protected override void performAnimation()
		{
			base.performAnimation();
			Vector2 l_targetSize = m_rootCanvasTransform.sizeDelta;
			Tween l_expandTweenSequence = m_tweeningTransform.DOSizeDelta(l_targetSize, m_expandTime, false);
			l_expandTweenSequence.SetEase(Ease.InOutQuad);

			Tween l_fadeInTween = DOTween.To(getAlpha, setAlpha, 1.0, m_fadeTime);
			l_fadeInTween.SetEase(Ease.InOutQuad);

			Sequence l_fadeSequence = DOTween.Sequence();
			l_fadeSequence.AppendInterval(m_timeBeforeFadeIn);
			l_fadeSequence.Append(l_fadeInTween);

			Tween l_headTransform = m_smallView.DOAnchorPosY(237.4f, m_animationTime, true);
			Tween l_backgroundTransform = m_compositeBack.DOAnchorPosY(237.4f, m_animationTime, true);
			Tween l_actionsTransform = m_actions.DOAnchorPosY(-281.5f, m_animationTime, true);

			Tween l_cardSizeTween = m_card.DOSizeDelta(l_targetSize, m_animationTime, false);

			m_sequence = DOTween.Sequence();
			m_sequence.Append(l_expandTweenSequence);
			m_sequence.Join(l_fadeSequence);
			m_sequence.Join(l_headTransform);
			m_sequence.Join(l_cardSizeTween);
			m_sequence.Join(l_actionsTransform);
			m_sequence.Join(l_backgroundTransform);
			m_sequence.AppendCallback(TweenPanelAnimation_onComplete);
			m_sequence.Play();
		}

		protected double getAlpha()
		{
			return m_tweeningCanvasGroup.alpha;
		}

		protected void setAlpha(double p_alpha)
		{
			m_tweeningCanvasGroup.alpha = (float) p_alpha;
		}
	}
}