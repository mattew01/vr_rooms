﻿using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public abstract class TweenPanelAnimation : UIPanelAnimation, IAnimateable
	{
		[SerializeField] protected RectTransform m_rootCanvasTransform;
		[SerializeField] protected RectTransform m_tweeningTransform;
		[SerializeField] protected CanvasGroup m_tweeningCanvasGroup;

		public override void animate()
		{
			base.animate();
			if (m_shouldResetBeforeAnimation)
			{
				reset();
			}
			performAnimation();
		}

		public override void finish()
		{
			if (null != m_sequence && m_sequence.IsActive() && m_sequence.IsPlaying())
			{
				m_sequence.Complete();
				m_sequence = null;
			}
			base.finish();
		}

		public override void stop()
		{
			if (null != m_sequence && m_sequence.IsActive() && m_sequence.IsPlaying())
			{
				m_sequence.Kill(false);
				m_sequence = null;
			}
			base.stop();
		}

		public override float getPercentage()
		{
			float l_progress = 0f;
			if (m_sequence != null)
			{
				l_progress = m_sequence.ElapsedPercentage();
				if (float.IsNaN(l_progress))
				{
					l_progress = 0;
				}
			}
			return l_progress;
		}

		protected void TweenPanelAnimation_onComplete()
		{
			stop();
			onAnimationComplete();
		}

		protected virtual void performAnimation()
		{
			stop();
			setTapGuard(true);
		}

		protected Sequence m_sequence;
	}
}