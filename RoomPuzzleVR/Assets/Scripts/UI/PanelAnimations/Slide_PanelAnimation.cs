﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class SlideTweenPanelAnimation : TweenPanelAnimation
	{
		[SerializeField] private Vector2 m_StartingPos;
		[SerializeField] private Vector2 m_EndingPos;

		public enum SlideMethod
		{
			TweenXOnly,
			TweenYOnly,
			TweenXY,
		}

		[SerializeField] private SlideMethod m_slidingMethod;
		[SerializeField] private Ease m_EasingMethod = Ease.InOutQuad;

		public override void reset()
		{
			Vector3 l_localPosition = m_tweeningTransform.anchoredPosition3D;

			switch (m_slidingMethod)
			{
				case SlideMethod.TweenXOnly:
					l_localPosition.x = m_StartingPos.x;
					break;
				case SlideMethod.TweenYOnly:
					l_localPosition.y = m_StartingPos.y;
					break;

			}
			m_tweeningTransform.anchoredPosition3D = l_localPosition;
		}

		protected override void performAnimation()
		{
			base.performAnimation();
			m_sequence = DOTween.Sequence();
			Tween l_tween;

			switch (m_slidingMethod)
			{
				case SlideMethod.TweenXOnly:
					l_tween = m_tweeningTransform.DOAnchorPosX(m_EndingPos.x, m_animationTime, false);
					l_tween.SetEase(m_EasingMethod);
					m_sequence.Append(l_tween);
					break;
				case SlideMethod.TweenYOnly:
					l_tween = m_tweeningTransform.DOAnchorPosY(m_EndingPos.y, m_animationTime, false);
					l_tween.SetEase(m_EasingMethod);
					m_sequence.Append(l_tween);
					break;
			}

			m_sequence.AppendCallback(TweenPanelAnimation_onComplete);
			m_sequence.Play();
		}
	}
}