﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class UserCardShrink_PanelAnimation : TweenPanelAnimation
	{
		[SerializeField] public float m_shrinkTime = 0.5f;
		[SerializeField] public float m_fadeTime = 0.25f;
		[SerializeField] public RectTransform m_smallView;
		[SerializeField] public RectTransform m_card;
		[SerializeField] public RectTransform m_compositeBack;
		[SerializeField] public RectTransform m_actions;

		public override void reset()
		{
			base.reset();
			Vector2 l_targetSize = m_rootCanvasTransform.sizeDelta;
			m_tweeningTransform.sizeDelta = l_targetSize;
			m_tweeningCanvasGroup.alpha = 1;
		}

		protected override void performAnimation()
		{
			base.performAnimation();
			Vector2 l_targetSize = Vector2.zero;
			Tween l_expandTweenSequence = m_tweeningTransform.DOSizeDelta(l_targetSize, m_shrinkTime, false);
			l_expandTweenSequence.SetEase(Ease.InOutQuad);

			Tween l_fadeOutTime = DOTween.To(getAlpha, setAlpha, 0.0, m_fadeTime);
			l_fadeOutTime.SetEase(Ease.InOutQuad);

			Tween l_headTransform = m_smallView.DOAnchorPosY(0, m_animationTime, true);
			Tween l_backgroundTransform = m_compositeBack.DOAnchorPosY(0, m_animationTime, true);
			Tween l_actionsTransform = m_actions.DOAnchorPosY(-446, m_animationTime, true);

			l_targetSize = m_smallView.sizeDelta;
			Tween l_cardSizeTween = m_card.DOSizeDelta(l_targetSize, m_animationTime, false);

			m_sequence = DOTween.Sequence();
			m_sequence.Append(l_expandTweenSequence);
			m_sequence.Join(l_fadeOutTime);
			m_sequence.Join(l_headTransform);
			m_sequence.Join(l_cardSizeTween);
			m_sequence.Join(l_backgroundTransform);
			m_sequence.Join(l_actionsTransform);
			m_sequence.AppendCallback(TweenPanelAnimation_onComplete);
			m_sequence.Play();
		}

		protected double getAlpha()
		{
			return m_tweeningCanvasGroup.alpha;
		}

		protected void setAlpha(double p_alpha)
		{
			m_tweeningCanvasGroup.alpha = (float) p_alpha;
		}
	}
}