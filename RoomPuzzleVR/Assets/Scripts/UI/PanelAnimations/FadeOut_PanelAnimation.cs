﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class FadeOut_PanelAnimation : TweenPanelAnimation
	{
		public override void reset()
		{
			m_tweeningCanvasGroup.alpha = 1;
		}

		protected override void performAnimation()
		{
			base.performAnimation();
			Tweener l_tweener = DOTween.To(getAlpha, setAlpha, 0.0, m_animationTime);
			Sequence l_sequence = DOTween.Sequence();
			l_sequence.Append(l_tweener);
			l_sequence.AppendCallback(TweenPanelAnimation_onComplete);
			l_sequence.Play();
		}

		protected double getAlpha()
		{
			return m_tweeningCanvasGroup.alpha;
		}

		protected void setAlpha(double p_alpha)
		{
			m_tweeningCanvasGroup.alpha = (float) p_alpha;
		}
	}
}