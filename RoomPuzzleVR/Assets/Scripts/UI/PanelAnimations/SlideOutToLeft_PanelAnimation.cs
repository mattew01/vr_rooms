﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class SlideOutToLeft_PanelAnimation : TweenPanelAnimation
	{
		public override void reset()
		{
			base.reset();
			Vector3 l_localPosition = m_tweeningTransform.anchoredPosition3D;
			l_localPosition.x = 0;
			m_tweeningTransform.anchoredPosition3D = l_localPosition;
		}

		protected override void performAnimation()
		{
			base.performAnimation();
			float l_screenWidth = m_rootCanvasTransform.rect.width;
			Tween l_tween = m_tweeningTransform.DOAnchorPosX(-l_screenWidth, m_animationTime, false);
			l_tween.SetEase(Ease.InOutQuad);

			m_sequence = DOTween.Sequence();
			m_sequence.Append(l_tween);
			m_sequence.AppendCallback(TweenPanelAnimation_onComplete);
			m_sequence.Play();
		}
	}
}