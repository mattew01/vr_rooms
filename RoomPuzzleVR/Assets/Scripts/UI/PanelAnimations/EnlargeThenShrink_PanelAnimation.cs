﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{

	public sealed class EnlargeThenShrink_PanelAnimation : TweenPanelAnimation
	{
		[SerializeField] private Vector3 m_enlargedSize;
		[SerializeField] private Vector3 m_shrinkSize;

		public override void reset()
		{
			m_tweeningTransform.localScale = Vector3.one;
		}

		protected override void performAnimation()
		{
			base.performAnimation();

			m_sequence = DOTween.Sequence();

			Tween l_tweenLarge = m_tweeningTransform.DOScale(m_enlargedSize, m_animationTime);
			l_tweenLarge.SetEase(Ease.InOutQuad);
			m_sequence.Append(l_tweenLarge);

			Tween l_tweenSmall = m_tweeningTransform.DOScale(m_shrinkSize, m_animationTime * 2f);
			l_tweenSmall.SetEase(Ease.InOutQuad);
			m_sequence.Append(l_tweenSmall);

			m_sequence.AppendCallback(TweenPanelAnimation_onComplete);
			m_sequence.Play();
		}
	}
}