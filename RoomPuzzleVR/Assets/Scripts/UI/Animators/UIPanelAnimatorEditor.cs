﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
    [UnityEditor.CustomEditor(typeof(UIPanelAnimator))]
    public class UIPanelAnimatorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            UIPanelAnimator l_animator = (UIPanelAnimator) target;

            if (l_animator.HasAnimationShowPanel || l_animator.HasAnimationDismissPanel)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("EDITOR: Test an Animation while in Play mode", EditorStyles.boldLabel);
                if (l_animator.HasAnimationShowPanel)
                {
                    if (GUILayout.Button("Animate Show Panel"))
                    {
                        l_animator.animateShow();
                    }
                }
                if (l_animator.HasAnimationDismissPanel)
                {
                    if (GUILayout.Button("Animate Dismiss Panel"))
                    {
                        l_animator.animateDismiss();
                    }
                }
            }
        }
    }
}
#endif