﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	/// <summary>
	/// Play a sequence of UI Panel Animations in a particular order 
	/// </summary>
	public sealed class SequentialAnimator : UIAnimator
	{
		//---------------------------------------------------------------------
		// EVENTS
		//---------------------------------------------------------------------
		public event System.Action OnAllAnimationsCompleted;

		//---------------------------------------------------------------------
		// FIELDS
		//---------------------------------------------------------------------
		[SerializeField] private bool m_animateOnStart;
		[SerializeField] private List<UIPanelAnimation> m_PanelAnimationList = new List<UIPanelAnimation>();

		//---------------------------------------------------------------------
		// METHODS
		//---------------------------------------------------------------------

		public void LateUpdate()
		{
			if (m_animateOnStart)
			{
				animateSequence();
				m_animateOnStart = false;
			}
		}

		/// <summary>
		/// Start the animation sequence
		/// </summary>
		public void animateSequence()
		{
			int l_count = m_PanelAnimationList.Count;
			if (l_count <= 0)
			{
				OnAllAnimationsCompleted.Invoke();
				return;
			}

			m_PanelAnimationList[l_count - 1].addOnFinishCallback(OnAllAnimationsCompleted);

			for (int i = l_count - 2; i >= 0; i--)
			{
				m_PanelAnimationList[i].addOnFinishCallback(m_PanelAnimationList[i + 1].animate);
			}

			m_PanelAnimationList[0].animate();
		}

		/// <summary>
		/// call finish() on all animations in the sequence
		/// </summary>
		public override void finishAll()
		{
			int l_animationCount = m_PanelAnimationList.Count;
			for (int i = l_animationCount - 1; i >= 0; --i)
			{
				UIPanelAnimation l_panelAnimation = m_PanelAnimationList[i];
				l_panelAnimation.finish();
			}
		}

		/// <summary>
		/// call stop() on all animations in the sequence
		/// </summary>
		public override void stopAll()
		{
			int l_animationCount = m_PanelAnimationList.Count;
			for (int i = l_animationCount - 1; i >= 0; --i)
			{
				UIPanelAnimation l_panelAnimation = m_PanelAnimationList[i];
				l_panelAnimation.stop();
			}
		}

		public override void resetAll()
		{
			int l_animationCount = m_PanelAnimationList.Count;
			for (int i = l_animationCount - 1; i >= 0; --i)
			{
				UIPanelAnimation l_panelAnimation = m_PanelAnimationList[i];
				l_panelAnimation.reset();
			}
		}

		/// <summary>
		/// get the overall percentage completed of the sequence of animations
		/// </summary>
		/// <returns>the total percentage</returns>
		public float getPercentage()
		{
			float l_progress = 0f;

			int l_animationCount = m_PanelAnimationList.Count;
			for (int i = l_animationCount - 1; i >= 0; --i)
			{
				UIPanelAnimation l_panelAnimation = m_PanelAnimationList[i];
				l_progress += l_panelAnimation.getPercentage();
			}

			if (l_animationCount > 0)
			{
				l_progress /= l_animationCount;
			}

			return l_progress;
		}

		/// <summary>
		/// Get an UIPanel from the Sequence by index
		/// </summary>
		/// <param name="index">Location in the sequence</param>
		/// <returns>the UIPanelAnimation reference</returns>
		public UIPanelAnimation GetPanelAnimation(int index)
		{
			if (index < m_PanelAnimationList.Count)
			{
				return m_PanelAnimationList[index];
			}
			else return null;
		}

		/// <summary>
		/// Add an UIPanelAnimation to the sequence at a location or at the end. 
		/// - Leaving out the index will place the Animation at the end
		/// </summary>
		/// <param name="p_animation">the animation to add to the list</param>
		/// <param name="p_index">-1 for the end, any valid index for a different place in the list</param>
		public void SetPanelAnimation(UIPanelAnimation p_animation, int p_index = -1)
		{
			if (p_index == -1)
			{
				m_PanelAnimationList.Add(p_animation);
			}
			else
			{
				m_PanelAnimationList.Insert(p_index, p_animation);
			}
		}

		/// <summary>
		/// set if the animation sequence should animate when enabled
		/// </summary>
		/// <param name="value">the value to set</param>
		public void setAnimateAtStart(bool value)
		{
			m_animateOnStart = value;
		}

		/// <summary>
		/// Add a callback when the sequence is complete
		/// </summary>
		/// <param name="p_action"></param>
		public void addOnAllAnimationsCompletedCallback(System.Action p_action)
		{
			OnAllAnimationsCompleted += p_action;
		}

		/// <summary>
		/// Remove a callback
		/// </summary>
		/// <param name="p_action"></param>
		public void removeOnAllAnimationsCompletedCallback(System.Action p_action)
		{
			OnAllAnimationsCompleted -= p_action;
		}

	}
}