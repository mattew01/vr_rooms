﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public abstract class UIAnimator : MonoBehaviour
	{
		public abstract void stopAll();
		public abstract void finishAll();
		public abstract void resetAll();
	}
}