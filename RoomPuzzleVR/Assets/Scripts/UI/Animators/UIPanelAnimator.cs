﻿// ============================================================================
// File:        PanelAnimationController.cs
// Created:     Thu Mar 07 2019
// Author(s):   Will Matterer <wmatterer@GameCloudStudios.com>
// ----------------------------------------------------------------------------
// Last Editor: Will Matterer
// Last Edit:   Tue Mar 12 2019
// ----------------------------------------------------------------------------
// Copyright (c) 2016-2019 GameCloud Studios, Inc.
// ============================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class UIPanelAnimator : UIAnimator
	{
		[SerializeField] private bool m_animateOnShow;
		[SerializeField] private UIPanelAnimation m_animateShowPanel;
		[SerializeField] private UIPanelAnimation m_animateDismissPanel;

		public event System.Action onShowPanelAnimationCompleted;
		public event System.Action onDismissPanelAnimationCompleted;

		public void LateUpdate()
		{
			if (m_animateOnShow)
			{
				animateShow();
				m_animateOnShow = false;
			}
		}

		public void animateShow()
		{
			stopAll();
			if (null != m_animateShowPanel)
			{
				m_animateShowPanel.addOnFinishCallback(onShowPanelAnimationCompleted);
				m_animateShowPanel.animate();
			}
			else
			{
				onShowPanelAnimationCompleted.Invoke();
			}
		}

		public void animateDismiss()
		{
			stopAll();
			if (null != m_animateDismissPanel)
			{
				m_animateDismissPanel.addOnFinishCallback(onDismissPanelAnimationCompleted);
				m_animateDismissPanel.animate();
			}
			else
			{
				onDismissPanelAnimationCompleted.Invoke();
			}
		}

		public override void stopAll()
		{
			if (null != m_animateShowPanel)
				m_animateShowPanel.stop();

			if (null != m_animateDismissPanel)
				m_animateDismissPanel.stop();

		}

		public override void finishAll()
		{
			if (null != m_animateShowPanel)
				m_animateShowPanel.finish();

			if (null != m_animateDismissPanel)
				m_animateDismissPanel.finish();

		}

		public override void resetAll()
		{
			if (null != m_animateShowPanel)
				m_animateShowPanel.reset();

			if (null != m_animateDismissPanel)
				m_animateDismissPanel.reset();

		}

		public void resetAnimateShowPanel()
		{
			if (null != m_animateShowPanel)
			{
				m_animateShowPanel.reset();
			}
		}

		public void resetAnimateDismissPanel()
		{
			if (null != m_animateDismissPanel)
			{
				m_animateDismissPanel.reset();
			}
		}

		public bool HasAnimationShowPanel { get { return null != m_animateShowPanel; } }
		public bool HasAnimationDismissPanel { get { return null != m_animateDismissPanel; } }
	}

}