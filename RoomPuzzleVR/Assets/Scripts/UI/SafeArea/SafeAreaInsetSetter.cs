﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.GameCloud.Controllers.UI
{
	public class SafeAreaInsetSetter : SafeAreaProxyBase 
	{
		public virtual void Start()
		{
			checkInsetsUpdate();
		}

		public virtual void Update() 
		{
			checkInsetsUpdate();
		}

		protected void checkInsetsUpdate()
		{
			if (updateSafeAreaInsets())
			{
				setInsets();
			}
		}

		protected virtual void setInsets()
		{
			if (isUsingLandscapeInsets() || isUsingPortraitInsets())
			{
				m_thisTransform.offsetMin = new Vector2(m_insetLeft, m_insetBottom);
				m_thisTransform.offsetMax = new Vector2(-m_insetRight, -m_insetTop);
			}
			else
			{
				m_thisTransform.offsetMin = Vector2.zero;
				m_thisTransform.offsetMax = Vector2.zero;
			}
		}
	}
}