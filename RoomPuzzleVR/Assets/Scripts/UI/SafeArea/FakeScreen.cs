﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class FakeScreen : MonoBehaviour
	{
		public static Rect safeArea;

		public Rect safeAreaSetter;
		public bool m_reset;
		public int m_padding = 50;
		public int m_screenWidth;
		public int m_screenHeight;

		public void Start()
		{
			resetSafeArea();
		}

		public void Update()
		{
			if (m_screenWidth != Screen.width || m_screenHeight != Screen.height || m_reset)
			{
				resetSafeArea();
			}

			safeArea = safeAreaSetter;

			m_screenWidth = Screen.width;
			m_screenHeight = Screen.height;
			m_reset = false;
		}

		public void resetSafeArea()
		{
			safeAreaSetter = new Rect(m_padding, m_padding, Screen.width-(2 * m_padding), Screen.height-(2 * m_padding));
		}

		public static void init()
		{
			if (!m_isInitialized)
			{
				FakeScreen l_fakeScreen = GameObject.FindObjectOfType<FakeScreen>();
				if (null != l_fakeScreen)
				{
					l_fakeScreen.Start();
					l_fakeScreen.Update();
				}
				m_isInitialized = true;
			}
		}

		private static bool m_isInitialized = false;
	}
}