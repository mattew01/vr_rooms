﻿using System.Collections.Generic;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class SafeAreaBase : MonoBehaviour 
	{
		public Rect getSafeArea()
		{
			#if UNITY_EDITOR
			FakeScreen.init();
			return FakeScreen.safeArea;
			#else
			return Screen.safeArea;
			#endif
		}
	}
}