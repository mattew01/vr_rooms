﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI.SafeArea
{
	public class ApplicationFooterSafeAreaSetter : SafeAreaInsetSetter 
	{
		public override void Start()
		{
			Vector2 l_sizeDelta = m_thisTransform.sizeDelta;
			m_baseHeight = l_sizeDelta.y;
			base.Start();
		}

		public override void Update()
		{
			base.Update();
		}

		protected override void setInsets()
		{
			Vector2 l_sizeDelta = m_thisTransform.sizeDelta;
			l_sizeDelta.y = m_baseHeight + m_insetTop;
			m_thisTransform.sizeDelta = l_sizeDelta;
		}

		private float m_baseHeight;
	}
}