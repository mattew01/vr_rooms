﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.GameCloud.Controllers.UI
{
	public class SafeAreaInsetScaler : SafeAreaProxyBase 
	{
		[HeaderAttribute("Scaling Options")]
		[SerializeField]
		public bool m_preserveVerticalSpace = false;
		[SerializeField]
		public bool m_preserveHorizontalSpace = false;

		[HeaderAttribute("Scaling Debug Information")]
		[SerializeField]
		public float m_scale;
		[SerializeField]
		public float m_inverseHorizontalInset;
		[SerializeField]
		public float m_inverseVerticalInset;


		public void Start()
		{
			checkInsetsUpdate();
		}

		public void Update() 
		{
			checkInsetsUpdate();
		}

		private void checkInsetsUpdate()
		{
			if (updateSafeAreaInsets())
			{
				setInsets();
			}
		}

		private void setInsets()
		{
			if (isUsingLandscapeInsets() || isUsingPortraitInsets())
			{
				applyScale();
				calculateInverseHorizontalInset();
				calculateInverseVerticalInset();
				setInverseInsets();
			}
			else
			{
				m_thisTransform.localScale = Vector3.one;
			}
		}

		private void applyScale()
		{
			float l_horizontalInsets = m_insetLeftU + m_insetRightU;
			float l_verticalInsets = m_insetTopV + m_insetBottomV;
			float l_horizontalScale = 1 - l_horizontalInsets;
			float l_VerticalScale = 1 - l_verticalInsets;
			float l_scale = Mathf.Min(l_horizontalScale, l_VerticalScale);
			m_thisTransform.localScale = new Vector3(l_scale, l_scale, l_scale);
			m_scale = l_scale;
		}

		private void calculateInverseHorizontalInset()
		{
			if ((m_useTopInset || m_useBottomInset) && m_preserveHorizontalSpace)
			{
				float l_width = m_parentWidth;
				float l_inverseInset = -l_width * (1f - m_scale) * 0.5f;

				if (m_useTopInset != m_useBottomInset)
				{
					l_inverseInset *= 0.5f;
				}

				if (m_scale != 0)
				{
					l_inverseInset /= m_scale;
				}

				m_inverseHorizontalInset = l_inverseInset;
			}
		}

		private void calculateInverseVerticalInset()
		{
			if ((m_useLeftInset || m_useRightInset || true) && m_preserveVerticalSpace)
			{
				float l_height = m_parentHeight;
				float l_inverseInset = -l_height * (1f - m_scale) * 0.5f;

				if (m_useLeftInset != m_useRightInset)
				{
					l_inverseInset *= 0.5f;
				}

				if (m_scale != 0)
				{
					l_inverseInset /= m_scale;
				}

				m_inverseVerticalInset = l_inverseInset;
			}
		}

		private void setInverseInsets()
		{
			if (m_preserveHorizontalSpace || m_preserveVerticalSpace)
			{
				m_thisTransform.offsetMin = new Vector2(m_inverseHorizontalInset, m_inverseVerticalInset);
				m_thisTransform.offsetMax = new Vector2(-m_inverseHorizontalInset, -m_inverseVerticalInset);
			}
		}
	}
}