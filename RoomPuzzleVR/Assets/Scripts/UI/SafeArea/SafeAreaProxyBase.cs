﻿using System.Collections.Generic;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class SafeAreaProxyBase : SafeAreaBase 
	{
		[HeaderAttribute("Transforms")]
		[SerializeField]
		protected RectTransform m_thisTransform;
		[SerializeField]
		protected RectTransform m_parentTransform;

		[HeaderAttribute("Orientations")]
		[SerializeField]
		public bool m_setInsetsForLandscape = true;
		[SerializeField]
		public bool m_setInsetsForPortrait = true;

		[HeaderAttribute("Insets Enabled")]
		[SerializeField]
		public bool m_useTopInset = true;
		[SerializeField]
		public bool m_useBottomInset = true;
		[SerializeField]
		public bool m_useLeftInset = true;
		[SerializeField]
		public bool m_useRightInset = true;

		protected bool updateSafeAreaInsets()
		{
			bool l_hasChanged = getParentDimensions();
			l_hasChanged |= convertSafeAreaToPercentages();
			if (l_hasChanged)
			{
				convertPercentagesToInsets();
			}
			return l_hasChanged;
		}

		protected bool getParentDimensions()
		{
			Vector2 l_sizeDelta = m_parentTransform.sizeDelta;
			bool l_hasChanged = false;

			if (l_sizeDelta.x == 0 || l_sizeDelta.y == 0)
			{
				Rect l_parentRect = m_parentTransform.rect;
				l_sizeDelta.x = l_parentRect.width;
				l_sizeDelta.y = l_parentRect.height;
			}

			if (m_parentWidth != l_sizeDelta.x 
				|| m_parentHeight != l_sizeDelta.y)
			{
				m_parentWidth = l_sizeDelta.x;
				m_parentHeight = l_sizeDelta.y;
				l_hasChanged = true;
			}

			return l_hasChanged;
		}

		protected bool convertSafeAreaToPercentages()
		{
			Rect l_safeArea = getSafeArea();
			bool l_hasChanged = !(l_safeArea.Equals(m_safeArea));

			if (l_hasChanged)
			{
				m_safeArea = getSafeArea();
				m_insetTopV = m_safeArea.y / Screen.height;
				m_insetBottomV = (Screen.height - (m_safeArea.y + m_safeArea.height)) / Screen.height;
				m_insetLeftU = m_safeArea.x / Screen.width;
				m_insetRightU = (Screen.width - (m_safeArea.x + m_safeArea.width)) / Screen.width;

				if (!m_useTopInset)
				{
					m_insetTopV = 0;
				}
				if (!m_useBottomInset)
				{
					m_insetBottomV = 0;
				}
				if (!m_useLeftInset)
				{
					m_insetLeftU = 0;
				}
				if (!m_useRightInset)
				{
					m_insetRightU = 0;
				}
			}

			return l_hasChanged;
		}

		protected void convertPercentagesToInsets()
		{
			m_insetTop = m_insetTopV * m_parentHeight;
			m_insetBottom = m_insetBottomV * m_parentHeight;
			m_insetLeft = m_insetLeftU * m_parentWidth;
			m_insetRight = m_insetRightU * m_parentWidth;
		}

		protected bool isUsingLandscapeInsets()
		{
			return Screen.height < Screen.width && m_setInsetsForLandscape;
		}

		protected bool isUsingPortraitInsets()
		{
			return Screen.height > Screen.width && m_setInsetsForPortrait;
		}
			
		protected Rect m_safeArea;

		[HeaderAttribute("debug Information")]

		[SerializeField]
		protected float m_parentWidth;
		[SerializeField]
		protected float m_parentHeight;

		[SerializeField]
		protected float m_insetTop;
		[SerializeField]
		protected float m_insetBottom;
		[SerializeField]
		protected float m_insetLeft;
		[SerializeField]
		protected float m_insetRight;

		[SerializeField]
		protected float m_insetTopV;
		[SerializeField]
		protected float m_insetBottomV;
		[SerializeField]
		protected float m_insetLeftU;
		[SerializeField]
		protected float m_insetRightU;
	}
}