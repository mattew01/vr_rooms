﻿using System.Collections.Generic;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class SafeAreaSwitch : SafeAreaBase 
	{
		[SerializeField]
		private GameObject m_safeAreaScreen;
		[SerializeField]
		private GameObject m_noSafeAreaScreen;

		public void Start() 
		{
			m_isInitialized = false;
			updateActiveScreen();	
		}
		
		public void Update() 
		{
			updateActiveScreen();	
		}

		private void updateActiveScreen()
		{
			bool l_shouldUseSafeArea = isSafeAreaActive();
			setActiveScreen(l_shouldUseSafeArea);
		}

		private bool isSafeAreaActive()
		{
			Rect l_safeArea = getSafeArea();
			if (l_safeArea.width != Screen.width || l_safeArea.height != Screen.height)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private void setActiveScreen(bool p_useSafeArea)
		{
			if (p_useSafeArea != m_isSafeAreaActive || !m_isInitialized)
			{
				m_safeAreaScreen.SetActive(p_useSafeArea);
				m_noSafeAreaScreen.SetActive(!p_useSafeArea);
				m_isSafeAreaActive = p_useSafeArea;
				m_isInitialized = true;
			}
		}

		private bool m_isSafeAreaActive;
		private bool m_isInitialized;
	}
}