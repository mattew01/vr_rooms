﻿using System.Collections.Generic;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class SafeAreaHeaderResizer : SafeAreaProxyBase 
	{
		public void Start()
		{
			getBaseHeight();
		}

		public void Update() 
		{
			if (updateSafeAreaInsets())
			{
				setHeight();
			}
		}

		public void setHeight()
		{
			Vector2 l_size = m_thisTransform.sizeDelta;
			l_size.y = m_baseHeight;
			if (isUsingLandscapeInsets() || isUsingPortraitInsets())
			{
				l_size.y = m_insetTop + m_baseHeight;
			}
			else
			{
				l_size.y = m_baseHeight;
			}
			m_thisTransform.sizeDelta = l_size;
		}

		private void getBaseHeight()
		{
			Vector2 l_size = m_thisTransform.sizeDelta;
			m_baseHeight = l_size.y;
		}

		private float m_baseHeight;
	}
}