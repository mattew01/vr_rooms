﻿using System.Collections;
using System.Collections.Generic;
using Com.GameCloud.Controllers.UI;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Com.MWELive.UI
{
    public sealed class HealthBar_ObjectAnimation : UIObjectAnimation
    {
        [Header("Health Bar Object Animation")]
        [SerializeField] private Slider m_slider;

        public void AnimateValueChange(float p_value)
        {
            performAnimation();

            m_sequence = DOTween.Sequence();

            Tween l_slideTween = m_slider.DOValue(p_value, m_animationTime);
            l_slideTween.SetEase(Ease.InOutQuad);
            m_sequence.Append(l_slideTween);

            m_sequence.AppendCallback(onAnimationComplete);
            m_sequence.Play();
        }

        public void SetMaxValue(float p_value)
        {
            if (null != m_slider)
                m_slider.maxValue = p_value;
        }

        public override void reset()
        {
            base.reset();
            if (null != m_slider)
                m_slider.value = m_slider.maxValue;
        }
    }
}