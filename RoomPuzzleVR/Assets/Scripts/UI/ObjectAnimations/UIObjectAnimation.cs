﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{

	/// <summary>
	/// Animate a single object
	/// </summary>
	public abstract class UIObjectAnimation : MonoBehaviour, IAnimateable
	{
		[Header("UI Object Animation")]
		[SerializeField] protected float m_animationTime = 0.5f;
		[SerializeField] protected bool m_shouldResetBeforeAnimation = true;

		// ------------------ Life Cycle -----------------
		public virtual void animate()
		{
			if (m_shouldResetBeforeAnimation)
			{
				reset();
			}
			performAnimation();
		}

		public virtual void finish()
		{
			if (null != m_sequence && m_sequence.IsActive() && m_sequence.IsPlaying())
			{
				m_sequence.Complete();
				m_sequence = null;
			}
		}

		public virtual void stop()
		{
			if (null != m_sequence && m_sequence.IsActive() && m_sequence.IsPlaying())
			{
				m_sequence.Kill(false);
				m_sequence = null;
			}
		}

		public virtual void reset() { }

		public virtual void setAnimationTime(float p_time)
		{
			m_animationTime = p_time;
		}

		public virtual void setResetBeforeAnimate(bool p_isResetting)
		{
			m_shouldResetBeforeAnimation = p_isResetting;
		}

		public virtual void addOnFinishCallback(System.Action p_action)
		{
			m_onComplete += p_action;
		}

		public virtual void removeOnFinishCallback(System.Action p_action)
		{
			m_onComplete -= p_action;
		}

		public virtual float getPercentage()
		{
			float l_progress = 0f;
			if (m_sequence != null)
			{
				l_progress = m_sequence.ElapsedPercentage();
				if (float.IsNaN(l_progress))
				{
					l_progress = 0;
				}
			}
			return l_progress;
		}

		// ------------------ Protected -----------------

		protected void onAnimationComplete()
		{
			m_onComplete();
		}

		protected virtual void performAnimation()
		{
			stop();
		}

		protected event System.Action m_onComplete = delegate { };
		protected Sequence m_sequence;
	}
}