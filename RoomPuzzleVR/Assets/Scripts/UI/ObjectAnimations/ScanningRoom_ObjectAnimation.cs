﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{
	public class ScanningRoom_ObjectAnimation : UIObjectAnimation
	{
		[SerializeField] private Vector2[] tweenPositions;

		[SerializeField] protected RectTransform m_tweeningTransform;

		public override void reset()
		{
			m_tweeningTransform.anchoredPosition = tweenPositions[0];
		}

		protected override void performAnimation()
		{
			base.performAnimation();

			if (tweenPositions.Length < 2) return;

			m_sequence = DOTween.Sequence();
			for (int i = 1; i < tweenPositions.Length; i++)
			{
				Tween l_Tween = m_tweeningTransform.DOAnchorPos(tweenPositions[i], m_animationTime, false);
				if (i % 2 == 0)
					l_Tween.SetEase(Ease.InSine);
				else
					l_Tween.SetEase(Ease.OutSine);
				m_sequence.Append(l_Tween);
			}
			m_sequence.SetLoops(-1);
			m_sequence.AppendCallback(onAnimationComplete);
			m_sequence.Play();
		}

#if UNITY_EDITOR
		void OnDrawGizmos()
		{
			Gizmos.color = Color.blue;
			foreach (var pos in tweenPositions)
			{
				Gizmos.DrawSphere(m_tweeningTransform.position + new Vector3(pos.x, pos.y, 0), 8f);
			}
		}
#endif

	}

}