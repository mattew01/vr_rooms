﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Com.GameCloud.Controllers.UI
{

	public class FigureEight_ObjectAnimation : UIObjectAnimation
	{

		[SerializeField]
		private float strength = 100f;

		[SerializeField]
		protected RectTransform m_tweeningTransform;

		// private Fields
		private float t = 0f;
		private bool isAnimating = false;

		protected override void performAnimation()
		{
			base.performAnimation();
			isAnimating = true;
		}

		public override void reset()
		{
			base.reset();
			t = 0f;
		}

		public override void stop()
		{
			isAnimating = false;
		}

		public override void finish()
		{
			isAnimating = false;
		}

		void Update()
		{
			if (isAnimating)
			{
				t += Time.deltaTime;
				m_tweeningTransform.anchoredPosition = new Vector2(strength * Mathf.Sin(t), strength * Mathf.Sin(t) * Mathf.Cos(t));
			}

		}
	}
}