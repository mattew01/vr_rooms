﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Com.RoomPuzzleVR.UI
{

    public class LevelLoadPanelView : UIPanelView
    {
        [Header("Level Load Panel")]
        [SerializeField] private Button m_LevelLoadBtn;
        [SerializeField] private Text m_buttonText;


        public void setText(string p_text)
        {
            if (null != m_buttonText)
                m_buttonText.text = p_text;
        }

        public void AddListener_Button(UnityAction p_callback)
        {
            if (null != m_LevelLoadBtn)
                m_LevelLoadBtn.onClick.AddListener(p_callback);
        }

        public void RemoveAllListeners()
        {
            if (null != m_LevelLoadBtn)
                m_LevelLoadBtn.onClick.RemoveAllListeners();
        }

    }
}
