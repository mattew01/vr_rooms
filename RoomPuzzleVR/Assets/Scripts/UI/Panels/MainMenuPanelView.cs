﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Com.RoomPuzzleVR.UI
{
    public class MainMenuPanelView : UIPanelView
    {
        [Header("Main Menu View")]
        [SerializeField] private Button m_buttonA;
        [SerializeField] private Button m_buttonB;
        [SerializeField] private Button m_buttonC;
        [SerializeField] private Button m_buttonD;
        [SerializeField] private Button m_buttonE;
        [SerializeField] private Button m_buttonF;
        [SerializeField] private Button m_buttonG;
        [SerializeField] private Button m_buttonH;


    }

}