﻿using System.Collections;
using System.Collections.Generic;
using Com.RoomPuzzleVR.Systems;
using UnityEngine;

namespace Com.RoomPuzzleVR.UI
{
    public class MainMenuPanelController : UIPanelController
    {

        public override int getPanelType()
        {
            return (int) UIPanel.MainMenu;
        }

        public override void init(IServiceProvider p_serviceProvider)
        {
            base.init(p_serviceProvider);
        }

        public override void show()
        {
            base.show();
        }

        public override void dismiss()
        {
            base.dismiss();
        }

        

        public void LoadLevel(int p_level)
        {

        }
    }
}