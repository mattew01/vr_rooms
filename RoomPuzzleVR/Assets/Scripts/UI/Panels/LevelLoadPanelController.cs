﻿using Com.RoomPuzzleVR.Systems;
using UnityEngine;

namespace Com.RoomPuzzleVR.UI
{
    public class LevelLoadPanelController : UIPanelController
    {
        [SerializeField] private GameLevel m_LevelToLoad;

        [SerializeField] private GameObject m_doorObject;

        public override int getPanelType()
        {
            return (int)UIPanel.LevelLoad;
        }

        public override void init(IServiceProvider p_serviceProvider)
        {
            base.init(p_serviceProvider);

            LevelLoadPanelView l_view = (LevelLoadPanelView)m_view;
            l_view.AddListener_Button(LoadLevel);
            l_view.setText(m_LevelToLoad.ToString());

            m_doorObject.SetActive(false);
        }

        public override void show()
        {
            base.show();
            m_doorObject.SetActive(true);
        }

        public override void dismiss()
        {
            base.dismiss();
            m_doorObject.SetActive(false);
        }


        public void LoadLevel()
        {
            if (null != m_serviceProvider)
            {

                GameManager l_gm = m_serviceProvider.GetGameManager();
                if (null != l_gm)
                    l_gm.LoadLevel(m_LevelToLoad);
                else
                    Debug.Log("LoadLevel() called, but GameManager could not be found");
            }
        }
    }
}