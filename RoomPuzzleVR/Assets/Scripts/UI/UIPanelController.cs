﻿using System.Collections;
using System.Collections.Generic;
using Com.GameCloud.Controllers.UI;
using Com.RoomPuzzleVR.Systems;
using UnityEngine;

namespace Com.RoomPuzzleVR.UI
{
    /// <summary>
    /// UI Panel Controller
    /// -------------------
    /// Abstract base class for all UI Panel Controllers
    /// - inherit from this class in all UI Panel Controllers
    /// Works in concert with UI Panel View
    /// </summary>
    public abstract class UIPanelController : MonoBehaviour
    {
        // --------------------------------------------------------------------
        // FIELDS
        // --------------------------------------------------------------------
        [Header("UI Panel Controller")]
        [SerializeField] protected UIPanelView m_view;

        public event System.Action OnShowCompleteEvent;
        public event System.Action OnDismissCompleteEvent;

        public bool DoNotDestroy
        {
            get;
            set;
        }

        // --------------------------------------------------------------------
        // Public METHODS
        // --------------------------------------------------------------------
        ///  <summary>
        /// init(): call base.init() first
        /// </summary>
        /// <param name="p_manager"></param>
        public virtual void init(IServiceProvider p_serviceProvider)
        {
            m_serviceProvider = p_serviceProvider;
            m_view.SetActive(false);
        }

        ///  <summary>
        /// gets the panel ID
        /// </summary>
        /// <returns></returns>
        public abstract int getPanelType();

        ///  <summary>
        /// shows the panel (must call init() before show() can be called)
        /// </summary>
        public virtual void show()
        {
            if (null != m_view)
            {
                m_view.SetActive(true);
                UIPanelAnimator l_uiAnimator = (UIPanelAnimator) m_view.GetUIAnimator();
                if (null == l_uiAnimator)
                {
                    onShowAnimationComplete();
                    return;
                }

                l_uiAnimator.onShowPanelAnimationCompleted -= onShowAnimationComplete;
                l_uiAnimator.onShowPanelAnimationCompleted += onShowAnimationComplete;
                l_uiAnimator.animateShow();
            }
            else
            {
                onShowAnimationComplete();
            }
        }

        ///  <summary>
        /// call to refresh the UI panel
        /// </summary>
        public virtual void refresh()
        { }

        ///  <summary>
        /// dismisses the panel without destroying it.
        /// </summary>
        public virtual void dismiss()
        {
            if (null != m_view)
            {
                UIPanelAnimator l_uiAnimator = (UIPanelAnimator) m_view.GetUIAnimator();
                if (null == l_uiAnimator)
                {
                    onDismissAnimationComplete();
                    return;
                }

                l_uiAnimator.onDismissPanelAnimationCompleted -= onDismissAnimationComplete;
                l_uiAnimator.onDismissPanelAnimationCompleted += onDismissAnimationComplete;
                l_uiAnimator.animateDismiss();
            }
            else
            {
                onDismissAnimationComplete();
            }
        }

        /// <summary>
        /// Destroy():  call base.destroy() last
        /// </summary>
        public virtual void destroy()
        {
            if (null != m_view)
                m_view.SetActive(false);

            if (!DoNotDestroy)
                m_view = null;
        }

        // --------------------------------------------------------------------
        // Protected Methods
        // --------------------------------------------------------------------

        ///  <summary>
        /// on Dismiss Animation Complete : called after dismiss animation is completed
        /// </summary>
        protected virtual void onDismissAnimationComplete()
        {
            if (m_view != null) m_view.SetActive(false);
            if (null != OnDismissCompleteEvent)
                OnDismissCompleteEvent.Invoke();
        }

        ///  <summary>
        /// on Show Animation Complete : called after show animation is completed
        /// </summary>
        protected virtual void onShowAnimationComplete()
        {
            if (null != OnShowCompleteEvent)
                OnShowCompleteEvent.Invoke();
        }

        protected IServiceProvider m_serviceProvider;
    }
}