﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Systems
{
    public interface IServiceProvider
    {
        GameManager GetGameManager();
        UIManager GetUIManager();
    }
}