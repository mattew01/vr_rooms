﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Com.RoomPuzzleVR.UI;
using UnityEngine;

namespace Com.RoomPuzzleVR.Systems
{
    public enum UIPanel
    {
        UNKNOWN = -1,
        Generic = 0,
        MainMenu = 1,
        LevelLoad = 2,
    }

    public class UIManager : MonoBehaviour
    {
        public Camera m_uiCamera;
        public GameObject[] m_screenPrefabs;

        public void init(IServiceProvider p_provider)
        {
            m_serviceProvider = p_provider;
            m_uiPanels = new List<UIPanelController>();
        }

        public UIPanelController InitPanel(UIPanel p_screen)
        {
            GameObject l_screen = null;
            UIPanelController l_gameUI = null;
            GameObject l_screenPrefab = m_screenPrefabs[(int) p_screen];

            if (l_screenPrefab != null)
            {
                l_screen = GameObject.Instantiate(l_screenPrefab);
                Canvas l_canvas = l_screen.GetComponent<Canvas>();
                if (null != l_canvas)
                {
                    l_canvas.worldCamera = m_uiCamera;
                }
                else
                {
                    Canvas[] l_canvasArray = l_screen.GetComponentsInChildren<Canvas>();
                    int l_canvasCount = l_canvasArray.Length;
                    for (int i = 0; i < l_canvasCount; ++i)
                    {
                        l_canvas = l_canvasArray[i];
                        l_canvas.worldCamera = m_uiCamera;
                    }
                }

                l_gameUI = l_screen.GetComponent<UIPanelController>();
                if (null == l_gameUI)
                {
                    l_gameUI = l_screen.AddComponent<UIPanelController>();
                }

                l_gameUI.init(m_serviceProvider);
                m_uiPanels.Add(l_gameUI);
            }
            return l_gameUI;
        }

        public UIPanelController ShowPanel(UIPanel p_screen, System.Action p_onShowCompletedCallback = null)
        {

            UIPanelController l_screen = GetPanel(p_screen);
            if (null == l_screen)
            {
                l_screen = InitPanel(p_screen);
            }

            if (null != p_onShowCompletedCallback)
                l_screen.OnShowCompleteEvent += p_onShowCompletedCallback;

            l_screen.show();
            return l_screen;
        }

        public UIPanelController GetPanel(UIPanel p_screen)
        {
            UIPanelController l_returnScreen = null;
            for (int i = m_uiPanels.Count - 1; i >= 0; i--)
            {
                UIPanelController l_gameUI = m_uiPanels[i];
                if (null == l_gameUI) continue;
                if (l_gameUI.getPanelType() == (int) p_screen)
                {
                    l_returnScreen = l_gameUI;
                }
            }
            return l_returnScreen;
        }

        public GameObject GetPanelObject(UIPanel p_screen)
        {
            GameObject l_returnScreen = null;
            for (int i = m_uiPanels.Count - 1; i >= 0; i--)
            {
                UIPanelController l_gameUI = m_uiPanels[i];
                if (null == l_gameUI) continue;
                if (l_gameUI.getPanelType() == (int) p_screen)
                {
                    l_returnScreen = l_gameUI.gameObject;
                }
            }
            return l_returnScreen;
        }

        public void DismissPanel(UIPanel p_screen, System.Action p_onDismissCompletedCallback = null)
        {
            UIPanelController l_screen = GetPanel(p_screen);
            if (null == l_screen) return;

            if (null != p_onDismissCompletedCallback)
                l_screen.OnDismissCompleteEvent += p_onDismissCompletedCallback;

            l_screen.dismiss();
        }

        public void RemovePanel(UIPanel p_screen)
        {
            for (int i = m_uiPanels.Count - 1; i >= 0; i--)
            {
                UIPanelController l_screen = m_uiPanels[i];
                if (null == l_screen) continue;
                if (l_screen.getPanelType() == (int) p_screen)
                {
                    l_screen.destroy();
                    GameObject.Destroy(l_screen.gameObject);
                    m_uiPanels.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// clear all Panels
        /// </summary>
        public void DestroyAllPanels()
        {
            for (int i = m_uiPanels.Count - 1; i >= 0; i--)
            {
                UIPanelController l_screen = m_uiPanels[i];
                l_screen.destroy();
                GameObject.Destroy(l_screen.gameObject);
            }
            m_uiPanels.Clear();
        }

        private IServiceProvider m_serviceProvider;
        private List<UIPanelController> m_uiPanels;
    }
}