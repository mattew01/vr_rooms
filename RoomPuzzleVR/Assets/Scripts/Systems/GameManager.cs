﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Com.RoomPuzzleVR.Systems
{
    public sealed class GameManager : MonoBehaviour, IServiceProvider
    {
        [Header("Game Manager")]
        [SerializeField] private UIManager m_uiManager;
        [SerializeField] private GameDataModel m_GameData;


        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);

            m_uiManager.init(this);

            LoadLevel(GameLevel.Exit);
        }


        public GameManager GetGameManager()
        {
            return this;
        }

        public UIManager GetUIManager()
        {
            return m_uiManager;
        }
        

        public void LoadLevel(GameLevel p_level)
        {
            string name = m_GameData.LevelNameList[(int)p_level];

            SceneManager.LoadScene(name, LoadSceneMode.Single);
        }
    }


}
