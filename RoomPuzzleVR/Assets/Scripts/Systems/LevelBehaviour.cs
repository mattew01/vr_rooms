﻿using UnityEngine;

namespace Com.RoomPuzzleVR.Systems
{
    public abstract class LevelBehaviour : MonoBehaviour
    {
        
        [SerializeField] private bool m_Active = false;

        public bool IsActiveInLevel
        {
            get
            {
                return m_Active;
            }
        }

        public bool IsNotActiveInLevel
        {
            get
            {
                return !m_Active;
            }
        }

        public virtual void OnLevelBegin()
        {
            m_Active = true;
        }

        public virtual void OnLevelEnd()
        {
            m_Active = false;
        }
    }
}
