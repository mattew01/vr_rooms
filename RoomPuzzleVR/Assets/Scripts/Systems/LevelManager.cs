﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.RoomPuzzleVR.UI;
using Com.RoomPuzzleVR.Gameplay;

namespace Com.RoomPuzzleVR.Systems
{
    public sealed class LevelManager : MonoBehaviour
    {
        [Header("NOTE: Please do not edit unless necessary")]
        [SerializeField] private bool m_StartLevelOnAwake = true;
        [SerializeField] private List<UIPanelController> m_worldUIPanels;
        [SerializeField] private bool m_showAllPanelsAtStart = false;
        [SerializeField] private GameMechanics m_ActiveMechanics;
        
        public void LevelStart()
        {
            if (null != m_levelBehaviors) { 
                foreach (var l_behavior in m_levelBehaviors)
                {
                    l_behavior.OnLevelBegin();
                }
            }

            foreach(var l_panel in m_worldUIPanels)
            {
                l_panel.init(m_gameManager);
                if(m_showAllPanelsAtStart)
                    l_panel.show();
            }

            if(null == m_ActiveMechanics)
                m_ActiveMechanics = GetComponent<GameMechanics>();

            if(null != m_ActiveMechanics)
                m_ActiveMechanics.StartLevel();
        }

        public void LevelFailure()
        {
            Debug.Log("Level Failed");
            if(null != m_ActiveMechanics)
                m_ActiveMechanics.EndLevel();

            if (null != m_levelBehaviors) { 
                foreach (var l_behavior in m_levelBehaviors)
                {
                    l_behavior.OnLevelEnd();
                }
            }

            StartCoroutine(reloadThisLevel());
        }

        public void LevelSuccess()
        {
            Debug.Log("Level Succeeded");
            if(null != m_ActiveMechanics)
            m_ActiveMechanics.EndLevel();

            if (null != m_levelBehaviors) { 
                foreach (var l_behavior in m_levelBehaviors)
                {
                    l_behavior.OnLevelEnd();
                }
            }
            
            foreach(var l_panel in m_worldUIPanels)
            {
                l_panel.show();
            }
        }

        public void LoadLevel(int p_level)
        {
            if(null == m_gameManager)
                m_gameManager = GameObject.FindObjectOfType<GameManager>();

            Debug.Log("LoadLevel: " + (GameLevel)p_level);
            m_gameManager.LoadLevel((GameLevel)p_level);
        }

        void Awake()
        {
            m_levelBehaviors = GameObject.FindObjectsOfType<LevelBehaviour>();
            m_gameManager = GameObject.FindObjectOfType<GameManager>();

            if (m_StartLevelOnAwake)
                LevelStart();
        }

        IEnumerator reloadThisLevel()
        {
            if(null != m_ActiveMechanics)
            {
                yield return new WaitForSeconds(m_ActiveMechanics.getReloadTime());
                m_gameManager.LoadLevel(m_ActiveMechanics.getLevel());
            }
            yield return null;
        }

        private LevelBehaviour[] m_levelBehaviors;

        private GameManager m_gameManager;
    }
}
