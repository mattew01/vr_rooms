﻿using System.Collections.Generic;
using UnityEngine;

namespace Com.RoomPuzzleVR.Systems
{

    [CreateAssetMenu(fileName = "GameData", menuName = "GameAssets/GameDataModel", order = 0), System.Serializable]
    public class GameDataModel : ScriptableObject
    {
        [SerializeField] protected List<string> m_LevelNameList = new List<string>();

        public List<string> LevelNameList
        {
            get { return m_LevelNameList; }
        }
    }


    public enum GameLevel
    {
        Exit = 0,
        A = 1,
        B = 2,
        C = 3,
        D = 4,
        E = 5,
        F = 6,
        G = 7,
        H = 8,
        I = 9
    }
}