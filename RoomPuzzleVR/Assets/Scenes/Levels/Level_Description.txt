LEVEL DESCRIPTION/WORK OUTLINE

2A) RING TOSS
Toss rings onto bottles.  
- needs to detect when a ring is placed on a bottle  (when it physically around a bottle, and has stopped moving)
- needs to destroy rings when they are missed
- instantiate rings after success/failed toss
- count number of successful ring tosses
- count number of rings used
- unlock level based on those counts
- rings are throwable
- no movement (locks them into position)

2B) FLOWER-ALTAR
Player collects "special" flowers and places them on an single altar
- Pickable Objects (objects are picked up by the player)
- Only one object can be carried in each hand
- Placeable objects (objects carried are placeable on an altar) --> changes the state of the object to "placed"
- After placing X objects, the level is unlocked
- player movement

2C) SIMON SAYS
Player follows along to a random pattern by clicking objects
- Clickable objects need to light up when (1) clicked and (2) selected by the AI
- a sequence needs to generated, adding one to the sequence each time
- the sequence needs to checked -- each clicked object needs to notify/listened to by the level manager
- if the player fails the sequence, it should reset & regenerate
- once the sequence reaches length of X, after a successful pass then unlock level
- no movement (locks them into position)

2D) PENALTY-KICK SOCCER
Player can kick a ball into a goal, with a goalie attempting to block the shot
- ball needs to detect that it's in the goal -> goal state
- detect missed shot -> dead state
- detect dead ball (no movement) -> stopped state
- goalie track (goalie is just a dummy) -- follows a pre-set path back & forth
- player applies a force to the ball
- ball using gravity & rigidbody physics
- ball needs to spawn after goal/dead ball
- aim line? line renderer
- no movement (locks them into position)
- AIM with controller, then trigger to launch (no interaction with ball)

2E) SAVING A PIG
Player finds a key that then unlocks a pig pen; which the player can then carry to a redetermined location
- pickable object (key, pig)
- unlock by key (animated?) -- need collider to detect when key is in range
- pig saved -- collider to indicate pig made it out
- player movement to search for the key & pig, and place objects

2F) COOKING with ROBOTS
Player adds igredients to a cooking pot until they kill the chef to win the level
- Pickable objects (food items)
- Cooking Pot (items are placed inside & are destroyed). 
- Robot chef demands items be added.  (randomly select items).
- Items need to instantated near the player for them to grab.  New items created after old items are destroyed
- Robot is destroyable.  
- Items are throwable?
- no movement (locks them into position)

2G) THROWING KNIVES
Player throws knives at targets/dummies
- dummies detect being hit
- knives are throwable
- when all dummies are hit, unlock level
- knives respawn after hitting target or are dead
- knives are dead when they leave the area.
- knives that stop moving are not dead (they should remain).
- spawn timer (only create knives within X seconds) so to keep the game playable.
- knive object pool of X number of knives total.
- no movement (locks them into position)

2H) BILLARDS
Player pushes/hits balls larger than the player into the pockets
- Billards use rigidbody physics (collisions & reactions)
- Player applies a force to the billards
- Billards heavier than normal? 
- Level unlocked when all the balls are in the pockets
- Player movement to get closer to a ball.


